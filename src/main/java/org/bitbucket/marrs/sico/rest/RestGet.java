package org.bitbucket.marrs.sico.rest;

import static com.google.common.net.HttpHeaders.ACCEPT;
import static java.util.Collections.emptyMap;
import static java.util.stream.Collectors.toList;
import static org.apache.http.HttpStatus.SC_MULTIPLE_CHOICES;
import static org.apache.http.HttpStatus.SC_OK;
import static org.apache.http.entity.ContentType.APPLICATION_JSON;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class RestGet {

    private static final Logger LOG = LoggerFactory.getLogger(RestGet.class);

    private static final String EOF = "\\A";

    private final URI baseUrl;

    public RestGet(URI baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String get() throws IOException {
        return get(emptyMap());
    }

    public String get(Map<String, ? extends Object> parameters) throws IOException {
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            URI queryURL = queryURL(parameters);
            HttpGet httpGet = new HttpGet(queryURL);
            httpGet.setHeader(ACCEPT, APPLICATION_JSON.toString());

            LOG.info("Querying: {}", queryURL);

            try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
                checkStatus(response);
                String responseString = readResponse(response);

                LOG.info("Retrieved: {}", responseString);

                return responseString;
            }
        }
    }

    private URI queryURL(Map<String, ? extends Object> parameters) {
        List<NameValuePair> parameterPairs = parameters.entrySet().stream()
                .map(entry -> new BasicNameValuePair(entry.getKey(), entry.getValue().toString()))
                .collect(toList());

        try {
            return new URIBuilder(baseUrl)
                    .addParameters(parameterPairs)
                    .build();
        } catch (URISyntaxException e) {
            throw new RuntimeException("Could not create query URL", e);
        }
    }

    private void checkStatus(CloseableHttpResponse response) throws IOException {
        int statusCode = response.getStatusLine().getStatusCode();
        if (SC_OK > statusCode || statusCode >= SC_MULTIPLE_CHOICES) {
            throw new IOException("Could not connect to server, response status: " + response.getStatusLine());
        }
    }

    private String readResponse(HttpResponse response) throws IOException {
        InputStream content = response.getEntity().getContent();
        try (Scanner contentScanner = new Scanner(content)) {
            contentScanner.useDelimiter(EOF);

            return contentScanner.hasNext() ? contentScanner.next() : "";
        }
    }

    public URI getBaseUrl() {
        return baseUrl;
    }
}