package org.bitbucket.marrs.sico.actor.lib.respond;

import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordOutputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createTypedInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createTypedOutputPort;

import org.bitbucket.marrs.sico.data.type.FuzzyWeightType;

import ptolemy.actor.TypedCompositeActor;
import ptolemy.actor.TypedIOPort;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.ComponentRelation;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

/**
 * Actor that chooses an action based on use intentions and involvement-distance
 * tradeoff input.
 * <p>
 * The actor calculates the expected satisfaction of the agent for each feature
 * and chooses the action associated with the feature that provides the highest
 * satisfaction values from the Actions input.
 */
public final class AffectiveDecision extends TypedCompositeActor {

    private static final String USE_INTENTIONS_IN = "useIntentionsInput";
    private static final String USE_INTENTIONS_IN_DISP = "Use intentions";
    private static final String TRADEOFF_IN = "tradeoffInput";
    private static final String TRADEOFF_IN_DISP = "Tradeoff";
    private static final String ENGAGEMENT_IN = "engagementInput";
    private static final String ENGAGEMENT_IN_DISP = "Engagement";
    private static final String ACTIONS_IN = "actionsInput";
    private static final String ACTIONS_IN_DISP = "Actions";
    private static final String ACTION_TENDENCIES_IN = "actionTendenciesInput";
    private static final String ACTION_TENDENCIES_IN_DISP = "Action Tendencies";

    private static final String ACTION_OUT = "actionOutput";
    private static final String ACTION_OUT_DISP = "Action";
    private static final String SATISFACTION_OUT = "satisfactionOutput";
    private static final String SATISFACTION_OUT_DISP = "Satisfaction";
    private static final String SATISFACTION_TOTAL_OUT = "satisfactionTotalOutput";
    private static final String SATISFACTION_TOTAL_OUT_DISP = "Satisfaction Total";
    private static final String FEATURE_SAT_OUT = "featureSatisfactionOutput";
    private static final String FEATURE_SAT_OUT_DISP = "Feature Satisfaction";

    private static final String SATISFACTION_ACTOR = "satisfaction";
    private static final String SATISFACTION_ACTOR_DISP = "Satisfaction";
    private static final String DECISION_ACTOR = "decision";
    private static final String DECISION_ACTOR_DISP = "Decision";

    private static final String SATISFACTION_WEIGHT_IN = "satisfactionWeightInput";
    private static final String SATISFACTION_WEIGHT_IN_DISP = "Satisfaction weight";
    private static final String DECISION_WEIGHT_MATRIX_IN = "decisionWeightMatrixInput";
    private static final String DECISION_WEIGHT_MATRIX_IN_DISP = "Decision weight matrix";
    private static final String DECISION_ROW_MAPPING_IN = "decisionRowMappingInput";
    private static final String DECISION_ROW_MAPPING_IN_DISP = "Decision row mapping";

    /**
     * Input port for the use intentions values. The input is of type
     * {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}.
     */
    public TypedIOPort useIntentionsInput;

    /**
     * Input port for the involvement-distance tradeoff values. The input is of
     * type {@link BaseType#RECORD} with values of type {@link BaseType#DOUBLE}.
     */
    public TypedIOPort tradeoffInput;

    /**
     * Input port for actions with maximum utility for a feature. The input
     * is of type {@link BaseType#RECORD} where the keys are feature names and
     * the values the names of the action that maximize the utility for that
     * feature.
     */
    public TypedIOPort actionsInput;

    /**
     * Input port for actions tendencies for the features. The input
     * is of type {@link BaseType#RECORD}.
     */
    public TypedIOPort actionTendenciesInput;

    /**
     * Input port for actions tendencies for the features. The input
     * is of type {@link BaseType#RECORD}.
     */
    public TypedIOPort engagementInput;

    /**
     * Output port for the selected action. The output is of type
     * {@link BaseType#STRING}.
     */
    public TypedIOPort actionOutput;

    /**
     * Output port for the satisfaction. The output is of type
     * {@link BaseType#RECORD} with values of type {@link Double}.
     */
    public TypedIOPort satisfactionOutput;

    /**
     * Output port for the total satisfaction. The output is of type
     * {@link BaseType#DOUBLE}.
     */
    public TypedIOPort satisfactionTotalOutput;
    public TypedIOPort featureSatisfactionOutput;

    /**
     * Input port for satisfaction weights.
     */
    public TypedIOPort satisfactionWeightInput;

    /**
     * Input port for the decision weights.
     */
    public TypedIOPort decisionWeightMatrixInput;

    /**
     * Input port for the row mapping of the {@code decisionWeightMatrixInput}.
     */
    public TypedIOPort decisionRowMappingInput;

    public Satisfaction satisfaction;
    public MaxSatisfactionDecision decision;

    public AffectiveDecision(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init();
    }

    public AffectiveDecision(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        useIntentionsInput = createRecordInputPort(this, USE_INTENTIONS_IN);
        useIntentionsInput.setDisplayName(USE_INTENTIONS_IN_DISP);
        tradeoffInput = createRecordInputPort(this, TRADEOFF_IN);
        tradeoffInput.setDisplayName(TRADEOFF_IN_DISP);
        engagementInput = createRecordInputPort(this, ENGAGEMENT_IN);
        engagementInput.setDisplayName(ENGAGEMENT_IN_DISP);
        actionsInput = createRecordInputPort(this, ACTIONS_IN);
        actionsInput.setDisplayName(ACTIONS_IN_DISP);
        actionTendenciesInput = createRecordInputPort(this, ACTION_TENDENCIES_IN);
        actionTendenciesInput.setDisplayName(ACTION_TENDENCIES_IN_DISP);

        actionOutput = createTypedOutputPort(this, ACTION_OUT, BaseType.STRING);
        actionOutput.setDisplayName(ACTION_OUT_DISP);
        satisfactionOutput = createRecordOutputPort(this, SATISFACTION_OUT);
        satisfactionOutput.setDisplayName(SATISFACTION_OUT_DISP);
        satisfactionTotalOutput = createTypedOutputPort(this, SATISFACTION_TOTAL_OUT, BaseType.DOUBLE);
        satisfactionTotalOutput.setDisplayName(SATISFACTION_TOTAL_OUT_DISP);
        featureSatisfactionOutput = createRecordOutputPort(this, FEATURE_SAT_OUT);
        featureSatisfactionOutput.setDisplayName(FEATURE_SAT_OUT_DISP);

        satisfactionWeightInput = createTypedInputPort(this, SATISFACTION_WEIGHT_IN, BaseType.DOUBLE);
        satisfactionWeightInput.setDisplayName(SATISFACTION_WEIGHT_IN_DISP);
        decisionWeightMatrixInput = createTypedInputPort(this, DECISION_WEIGHT_MATRIX_IN, BaseType.DOUBLE_MATRIX);
        decisionWeightMatrixInput.setDisplayName(DECISION_WEIGHT_MATRIX_IN_DISP);
        decisionRowMappingInput = createRecordInputPort(this, DECISION_ROW_MAPPING_IN);
        decisionRowMappingInput.setDisplayName(DECISION_ROW_MAPPING_IN_DISP);

        satisfaction = new Satisfaction(this, SATISFACTION_ACTOR);
        satisfaction.setDisplayName(SATISFACTION_ACTOR_DISP);

        decision = new MaxSatisfactionDecision(this, DECISION_ACTOR);
        decision.setDisplayName(DECISION_ACTOR_DISP);

        connect(satisfactionWeightInput, satisfaction.weightInput);
        connect(useIntentionsInput, satisfaction.useIntentionsInput);
        connect(tradeoffInput, satisfaction.tradeoffInput);

        connect(decisionWeightMatrixInput, decision.weightsInput);
        connect(decisionRowMappingInput, decision.rowMappingInput);
        connect(engagementInput, decision.engagementInput);
        connect(actionsInput, decision.actionInput);
        connect(actionTendenciesInput, decision.actionTendenciesInput);
        ComponentRelation satisfactionRelation = connect(satisfaction.output, decision.satisfactionInput);

        connect(actionOutput, decision.actionOutput);
        connect(featureSatisfactionOutput, decision.featureSatisfactionOutput);
        satisfactionOutput.link(satisfactionRelation);
        connect(satisfaction.outputAggregate, satisfactionTotalOutput);
    }
}
