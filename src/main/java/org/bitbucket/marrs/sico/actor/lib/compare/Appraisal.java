package org.bitbucket.marrs.sico.actor.lib.compare;

import static org.bitbucket.marrs.sico.actor.lib.base.DomainPort.AFFORDANCES;
import static org.bitbucket.marrs.sico.actor.lib.base.DomainPort.ETHICS;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createArrayInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordOutputPort;
import static org.bitbucket.marrs.sico.data.type.EdgeType.EDGE;

import org.bitbucket.marrs.sico.data.type.FuzzyWeightType;

import ptolemy.actor.TypedCompositeActor;
import ptolemy.actor.TypedIOPort;
import ptolemy.actor.lib.Discard;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.ComponentRelation;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

/**
 * Composite actor for the appraisal process.
 * <p>
 * The actor calculates Relevance and Valence based on the features of the
 * other agent, the encoded values of the ethics and affordances domains.
 * <p>
 * As a byproduct the actor provides the action with the highest utility value
 * for each feature.
 */
public final class Appraisal extends TypedCompositeActor {

    private static final String FEATURES_IN = "featuresInput";
    private static final String FEATURES_IN_DISP = "Features";

    private static final String VALENCE_OUT_TOTAL = "valenceOutputTotal";
    private static final String VALENCE_OUT_TOTAL_DISP = "Valence total";
    private static final String VALENCE_OUT = "valenceOutput";
    private static final String VALENCE_OUT_DISP = "Valence";

    private static final String RELEVANCE_OUT = "relevanceOutput";
    private static final String RELEVANCE_OUT_DISP = "Relevance";
    private static final String RELEVANCE_OUT_TOTAL = "relevanceOutputTotal";
    private static final String RELEVANCE_OUT_TOTAL_DISP = "Relevance total";

    private static final String UTILITY_OUTPUT = "utilitiesOutput";
    private static final String UTILITY_OUTPUT_DISP = "Utilities";
    private static final String MAX_UTILITY_ACTIONS_OUTPUT = "maxUtilityActionsOutput";
    private static final String MAX_UTILITY_ACTIONS_OUTPUT_DISP = "Actions";
    private static final String ACTION_TENDENCIES_OUTPUT = "actionTendenciesOutput";
    private static final String ACTION_TENDENCIES_OUTPUT_DISP = "Action tendencies";

    private static final String UTILITY_ACTOR = "utility";
    private static final String UTILITY_ACTOR_DISP = "Utility";
    private static final String RELEVANCE_ACTOR = "relevance";
    private static final String RELEVANCE_ACTOR_DISP = "Relevance";
    private static final String VALENCE_ACTOR = "valence";
    private static final String VALENCE_ACTOR_DISP = "Valence";
    private static final String ETHICS_DISCARD_ACTOR = "ethicsdisc";
    private static final String ETHICS_DISCARD_ACTOR_DISP = "Ethics Discard";

    private static final String FEATURE_TO_ACTION_INPUT = "featureToActionInput";
    private static final String FEATURE_TO_ACTION_INPUT_DISP = "Feature to action";
    private static final String GOAL_INPUT = "goalInput";
    private static final String GOAL_INPUT_DISP = "Goals";
    private static final String ACTION_TO_GOAL_INPUT = "actionToGoalInput";
    private static final String ACTION_TO_GOAL_INPUT_DISP = "Action to goal";
    private static final String ACTION_INPUT = "actionInput";
    private static final String ACTION_INPUT_DISP = "Actions";
    /**
     * Input port for the features. The port is of type {@link BaseType#RECORD}
     * with values of type {@link BaseType#DOUBLE}.
     */
    public TypedIOPort featuresInput;

    /**
     * Input port for the encoded values of the features in the ethics
     * domain. The input port is of type {@link BaseType#RECORD} with values of
     * type {@link FuzzyWeightType} containing the encoded weights of the
     * ethics domains.
     */
    public TypedIOPort ethicsInput;

    /**
     * Input port for the encoded values of the features in the affordances
     * domain. The input port is of type {@link BaseType#RECORD} with values of
     * type {@link FuzzyWeightType} containing the encoded weights of the
     * affordances domains.
     */
    public TypedIOPort affordancesInput;

    /**
     * Output port for the valence values of the features. The output port is of
     * type {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}.
     */
    public TypedIOPort valenceOutput;

    /**
     * Output port for the aggregate valence value. The output port is of type
     * {@link FuzzyWeightType}.
     */
    public TypedIOPort valenceOutputTotal;

    /**
     * Output port for the relevance values of the features. The output port is of
     * type {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}.
     */
    public TypedIOPort relevanceOutput;

    /**
     * Output port for the aggregate relevance value. The output port is of type
     * {@link FuzzyWeightType}.
     */
    public TypedIOPort relevanceOutputTotal;

    /**
     * Output port for the utility values of the features. The output
     * is of type {@link BaseType#RECORD} where the keys are feature names and
     * the values are fuzzy weights with the indicative and counter-indicative
     * utilities for that feature.
     */
    public TypedIOPort utilitiesOutput;

    /**
     * Output port for the actions with maximum utility for each feature. The output
     * port is of type {@link BaseType#RECORD} with values of type
     * {@link BaseType#STRING}. The values represent the action names.
     */
    public TypedIOPort maxUtilityActionsOutput;

    /**
     * Output port for the action tendencies (utilities) for each feature. The output
     * port is of type {@link BaseType#RECORD}. The values represent the action names
     * with the related utility.
     */
    public TypedIOPort actionTendenciesOutput;

    
    /**
     * Input port for feature - action associations. The input is of type
     * {@link BaseType#RECORD} where the keys are feature names and the values
     * are arrays of JSON strings that describe the associations of features
     * with actions. The JSON strings contain a JSON object describing the
     * feature in the property "concept1", a JSON object describing the action
     * in the property "concept2" and the truth value of the association in the
     * property "truthValue".
     */
    public TypedIOPort featureToActionInput;

    /**
     * Input port for data about the actions. The input is of type
     * {@link BaseType#RECORD} where the keys are action names and the values
     * are JSON strings that hold data about the actions. The JSON strings
     * contain a JSON object describing the action in the property "concept",
     * the name of the data in the property "relation" and a string
     * representation of the value of the data in the property "value".
     */
    public TypedIOPort actionInput;

    /**
     * Input port for action - goal associations. The input is of type
     * {@link BaseType#RECORD} where the keys are action names and the values
     * are arrays of JSON string tuples that describe the associations of
     * actions with the goals.
     * <p>
     * The first entry in the tuple is a JSON object that contains a JSON
     * object describing the action in the property "concept1", a JSON object
     * describing the goal in the property "concept2" and the truth value of
     * the association in the property "truthValue".
     * <p>
     * The second entry in the tuple is a JSON object that contains a JSON
     * object describing data related to the association. It contains a JSON
     * object describing the action in the property "concept1", a JSON object
     * describing the goal in the property "concept2", the name of the data in
     * the property "relation" and a string representation of the value of the
     * data in the property "value".
     */
    public TypedIOPort actionToGoalInput;

    /**
     * Input port for data about the goals. The input is of type
     * {@link BaseType#RECORD} where the keys are goal names and the values
     * are arrays of JSON strings that hold data about the goals. The JSON
     * strings contain a JSON object describing the goal in the property
     * "concept", the name of the data in the property "relation" and a string
     * representation of the value of the data in the property "value".
     */
    public TypedIOPort goalInput;


    public Utility utility;
    public Relevance relevance;
    public Valence valence;
    public Discard ethicsSink;

    public Appraisal(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init();
    }

    public Appraisal(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        featuresInput = createRecordInputPort(this, FEATURES_IN);
        featuresInput.setDisplayName(FEATURES_IN_DISP);

        ethicsInput = createRecordInputPort(this, ETHICS.inputName);
        ethicsInput.setDisplayName(ETHICS.inputDisplayName);

        affordancesInput = createRecordInputPort(this, AFFORDANCES.inputName);
        affordancesInput.setDisplayName(AFFORDANCES.inputDisplayName);

        valenceOutput = createRecordOutputPort(this, VALENCE_OUT);
        valenceOutput.setDisplayName(VALENCE_OUT_DISP);
        valenceOutputTotal = createRecordOutputPort(this, VALENCE_OUT_TOTAL);
        valenceOutputTotal.setDisplayName(VALENCE_OUT_TOTAL_DISP);

        relevanceOutput = createRecordOutputPort(this, RELEVANCE_OUT);
        relevanceOutput.setDisplayName(RELEVANCE_OUT_DISP);
        relevanceOutputTotal = createRecordOutputPort(this, RELEVANCE_OUT_TOTAL);
        relevanceOutputTotal.setDisplayName(RELEVANCE_OUT_TOTAL_DISP);

        utilitiesOutput = createRecordOutputPort(this, UTILITY_OUTPUT);
        utilitiesOutput.setDisplayName(UTILITY_OUTPUT_DISP);

        maxUtilityActionsOutput = createRecordOutputPort(this, MAX_UTILITY_ACTIONS_OUTPUT);
        maxUtilityActionsOutput.setDisplayName(MAX_UTILITY_ACTIONS_OUTPUT_DISP);
        actionTendenciesOutput = createRecordOutputPort(this, ACTION_TENDENCIES_OUTPUT);
        actionTendenciesOutput.setDisplayName(ACTION_TENDENCIES_OUTPUT_DISP);

        featureToActionInput = createArrayInputPort(this, FEATURE_TO_ACTION_INPUT, EDGE);
        featureToActionInput.setDisplayName(FEATURE_TO_ACTION_INPUT_DISP);
        actionInput = createRecordInputPort(this, ACTION_INPUT);
        actionInput.setDisplayName(ACTION_INPUT_DISP);
        actionToGoalInput = createArrayInputPort(this, ACTION_TO_GOAL_INPUT, EDGE);
        actionToGoalInput.setDisplayName(ACTION_TO_GOAL_INPUT_DISP);
        goalInput = createRecordInputPort(this, GOAL_INPUT);
        goalInput.setDisplayName(GOAL_INPUT_DISP);

        utility = new Utility(this, UTILITY_ACTOR);
        utility.setDisplayName(UTILITY_ACTOR_DISP);

        relevance = new Relevance(this, RELEVANCE_ACTOR);
        relevance.setDisplayName(RELEVANCE_ACTOR_DISP);
        valence = new Valence(this, VALENCE_ACTOR);
        valence.setDisplayName(VALENCE_ACTOR_DISP);

        ethicsSink = new Discard(this, ETHICS_DISCARD_ACTOR);
        ethicsSink.setDisplayName(ETHICS_DISCARD_ACTOR_DISP);

        ComponentRelation featureToActionConn = connect(featureToActionInput, utility.featureToActionInput);
        ComponentRelation actionConn = connect(actionInput, utility.actionInput);
        ComponentRelation actionToGoalConn = connect(actionToGoalInput, utility.actionToGoalInput);
        ComponentRelation goalConn = connect(goalInput, utility.goalInput);
        connect(utility.utilitiesOutput, utilitiesOutput);
        connect(utility.actionTendenciesOutput, actionTendenciesOutput);
        maxUtilityActionsOutput.link(actionConn);

        ComponentRelation featureConnection = connect(featuresInput, relevance.featuresInput);
        relevance.featureToActionInput.link(featureToActionConn);
        relevance.actionInput.link(actionConn);
        relevance.actionToGoalInput.link(actionToGoalConn);
        relevance.goalInput.link(goalConn);
        connect(relevance.output, relevanceOutput);
        connect(relevance.outputAggregate, relevanceOutputTotal);

        valence.featuresInput.link(featureConnection);
        connect(affordancesInput, valence.affordancesInput);
        valence.featureToActionInput.link(featureToActionConn);
        valence.actionInput.link(actionConn);
        valence.actionToGoalInput.link(actionToGoalConn);
        valence.goalInput.link(goalConn);
        connect(valence.output, valenceOutput);
        connect(valence.outputAggregate, valenceOutputTotal);

        connect(ethicsInput, ethicsSink.input);
    }
}