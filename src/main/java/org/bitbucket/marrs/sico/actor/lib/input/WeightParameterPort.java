package org.bitbucket.marrs.sico.actor.lib.input;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.function.Function;

import ptolemy.data.type.BaseType;
import ptolemy.data.type.RecordType;
import ptolemy.data.type.Type;

public enum WeightParameterPort {
    SIMILARITY_WEIGHTS("similarityWeights", "Similarity weights", BaseType.RECORD),
    DISSIMILARITY_WEIGHTS("dissimilarityWeights", "Dissimilarity weights", BaseType.RECORD),
    IDT_WEIGHT("idtWeight", "IDT weight", BaseType.DOUBLE),
    SATISFACTION_WEIGHT("satisfactionWeight", "Satisfaction weight", BaseType.DOUBLE),
    ENGAGMENT_WEIGHT_MATRIX("engagementWeightMatrix", "Engagement weight matrix", BaseType.DOUBLE_MATRIX),
    ENGAGEMENT_ROW_MAPPING("engagementRowMapping", "Engagement row mapping", BaseType.RECORD),
    USE_INTENTIONS_WEIGHT_MATRIX("useIntentionsWeightMatrix", "Use intentions weight matrix", BaseType.DOUBLE_MATRIX),
    USE_INTENTIONS_ROW_MAPPING("useIntentionsRowMapping", "Use intentions row mapping", BaseType.RECORD),
    DECISION_WEIGHT_MATRIX("decisionWeightMatrix", "Decision weight matrix", BaseType.DOUBLE_MATRIX),
    DECISION_ROW_MAPPING("decisionRowMapping", "Decision row mapping", BaseType.RECORD);

	private static final String[] STRING_ARRAY = new String[0];
    private static final Type[] TYPE_ARRAY = new Type[0];
	
    public final String name;
    public final String inputName;
    public final String inputNameTotal;
    public final String inputDisplayName;
    public final String inputDisplayNameTotal;
    public final String outputName;
    public final String outputNameTotal;
    public final String outputDisplayName;
    public final String outputDisplayNameTotal;
    public final Type type;

    WeightParameterPort(String fieldName, String displayName, Type type) {
        this.name = fieldName;
		this.type = type;
        this.inputName = fieldName + "Input";
        this.inputNameTotal = fieldName + "InputTotal";
        this.inputDisplayName = displayName;
        this.inputDisplayNameTotal = displayName + " total";
        this.outputName = fieldName + "Output";
        this.outputNameTotal = fieldName + "OutputTotal";
        this.outputDisplayName = displayName;
        this.outputDisplayNameTotal = displayName + " total";
    }

    public static Type getRecordType() {
        return new RecordType(names().toArray(STRING_ARRAY), types().toArray(TYPE_ARRAY));
    }
    
    public static List<String> names() {
    	return toNameList(port -> port.name);
    }

    public static List<String> inputNames() {
    	return toNameList(port -> port.inputName);
    }
    
    public static List<String> outputNames() {
    	return toNameList(port -> port.outputName);
    }

    public static List<String> inputNamesTotal() {
        return toNameList(port -> port.inputNameTotal);
    }

    private static List<String> toNameList(Function<WeightParameterPort, String> toNameFunction) {
        return asList(values()).stream()
                .map(toNameFunction)
                .collect(toList());
    }
    
    private static List<Type> types() {
    	return asList(values()).stream()
                .map(port -> port.type)
                .collect(toList());
    }
}