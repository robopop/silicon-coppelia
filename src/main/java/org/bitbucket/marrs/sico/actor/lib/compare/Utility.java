package org.bitbucket.marrs.sico.actor.lib.compare;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createArrayInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordOutputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.TokenConverter.DOUBLE_MAP_CONVERTER;
import static org.bitbucket.marrs.sico.actor.lib.util.TokenConverter.EDGE_LIST_CONVERTER;
import static org.bitbucket.marrs.sico.actor.lib.util.TokenConverter.STRING_MAP_CONVERTER;
import static org.bitbucket.marrs.sico.data.type.EdgeType.EDGE;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bitbucket.marrs.sico.algo.compare.ActionGoalTransition;
import org.bitbucket.marrs.sico.algo.compare.UtilityAlgo;
import org.bitbucket.marrs.sico.algo.compare.UtilityResult;
import org.bitbucket.marrs.sico.data.Edge;
import org.bitbucket.marrs.sico.data.FuzzyWeight;
import org.bitbucket.marrs.sico.data.token.FuzzyWeightToken;

import ptolemy.actor.TypedAtomicActor;
import ptolemy.actor.TypedIOPort;
import ptolemy.data.ArrayToken;
import ptolemy.data.ObjectToken;
import ptolemy.data.RecordToken;
import ptolemy.data.StringToken;
import ptolemy.data.Token;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

public class Utility extends TypedAtomicActor {
    private static final String FEATURE_TO_ACTION_IN = "featureToActionInput";
    private static final String FEATURE_TO_ACTION_IN_DISP = "Feature - Actions";
    private static final String ACTION_IN = "actionInput";
    private static final String ACTION_IN_DISP = "Actions";
    private static final String ACTION_TO_GOAL_IN = "actionToGoalInput";
    private static final String ACTION_TO_GOAL_IN_DISP = "Action - Goals";
    private static final String GOAL_IN = "goalInput";
    private static final String GOAL_IN_DISP = "Goals";

    private static final String TRANSITIONS_OUT = "transitionsOutput";
    private static final String TRANSITIONS_OUT_DISP = "Transitions";
    private static final String UTILITIES_OUT = "utilitiesOutput";
    private static final String UTILITIES_OUT_DISP = "Utilities";
    private static final String MAX_UTIL_ACTION_OUT = "maxUtilityActionOutput";
    private static final String MAX_UTIL_ACTION_OUT_DISP = "Max Utility Actions";
    private static final String ACTION_TENDENCIES_OUT = "actionTendenciesOutput";
    private static final String ACTION_TENDENCIES_OUT_DISP = "Action Tendencies";

    /**
     * Input port for feature - action associations. The port is of type
     * {@link BaseType#RECORD} where the keys are feature names and the values
     * are arrays of JSON strings that describe the associations of features
     * with actions. The JSON strings contain a JSON object describing the
     * feature in the property "concept1", a JSON object describing the action
     * in the property "concept2" and the truth value of the association in the
     * property "truthValue".
     */
    public TypedIOPort featureToActionInput;

    /**
     * Input port for data about the actions. The port is of type
     * {@link BaseType#RECORD} where the keys are action names and the values
     * are JSON strings that hold data about the actions. The JSON strings
     * contain a JSON object describing the action in the property "concept",
     * the name of the data in the property "relation" and a string
     * representation of the value of the data in the property "value".
     */
    public TypedIOPort actionInput;

    /**
     * Input port for action - goal associations. The port is of type
     * {@link BaseType#RECORD} where the keys are action names and the values
     * are arrays of JSON string tuples that describe the associations of
     * actions with the goals.
     * <p>
     * The first entry in the tuple is a JSON object that contains a JSON
     * object describing the action in the property "concept1", a JSON object
     * describing the goal in the property "concept2" and the truth value of
     * the association in the property "truthValue".
     * <p>
     * The second entry in the tuple is a JSON object that contains a JSON
     * object describing data related to the association. It contains a JSON
     * object describing the action in the property "concept1", a JSON object
     * describing the goal in the property "concept2", the name of the data in
     * the property "relation" and a string representation of the value of the
     * data in the property "value".
     */
    public TypedIOPort actionToGoalInput;

    /**
     * Input port for data about the goals. The port is of type
     * {@link BaseType#RECORD} where the keys are goal names and the values
     * are arrays of JSON strings that hold data about the goals. The JSON
     * strings contain a JSON object describing the goal in the property
     * "concept", the name of the data in the property "relation" and a string
     * representation of the value of the data in the property "value".
     */
    public TypedIOPort goalInput;

    /**
     * Output port for transitions from actions to goals. The output is of type
     * {@link BaseType#RECORD} where the keys are action names and the values
     * are arrays of {@link ActionGoalTransition} object tokens. The contained
     * actions are restricted to the actions that have maximum utility for at
     * least one feature.
     */
    public TypedIOPort transitionsOutput;

    /**
     * Output port for actions with maximum utility for a feature. The output
     * is of type {@link BaseType#RECORD} where the keys are feature names and
     * the values are the names of the action that maximize the utility for that
     * feature.
     */
    public TypedIOPort maxUtilityActionOutput;

    /**
     * Output port for the utility values of the features. The output
     * is of type {@link BaseType#RECORD} where the keys are feature names and
     * the values are fuzzy weights with the indicative and counter-indicative
     * utilities for that feature.
     */
    public TypedIOPort utilitiesOutput;

    /**
     * Output port for action tendencies of the features. The output is of type
     * {@link BaseType#RECORD} where the keys are feature names and the values
     * are of type {@link BaseType#RECORD} containing the action tendencies by
     * name of the action type.
     */
    public TypedIOPort actionTendenciesOutput;

    public Utility(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init();
    }

    public Utility(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        featureToActionInput = createArrayInputPort(this, FEATURE_TO_ACTION_IN, EDGE);
        featureToActionInput.setDisplayName(FEATURE_TO_ACTION_IN_DISP);

        actionInput = createRecordInputPort(this, ACTION_IN);
        actionInput.setDisplayName(ACTION_IN_DISP);

        actionToGoalInput = createArrayInputPort(this, ACTION_TO_GOAL_IN, EDGE);
        actionToGoalInput.setDisplayName(ACTION_TO_GOAL_IN_DISP);

        goalInput = createRecordInputPort(this, GOAL_IN);
        goalInput.setDisplayName(GOAL_IN_DISP);

        transitionsOutput = createRecordOutputPort(this, TRANSITIONS_OUT);
        transitionsOutput.setDisplayName(TRANSITIONS_OUT_DISP);

        utilitiesOutput = createRecordOutputPort(this, UTILITIES_OUT);
        utilitiesOutput.setDisplayName(UTILITIES_OUT_DISP);

        maxUtilityActionOutput = createRecordOutputPort(this, MAX_UTIL_ACTION_OUT);
        maxUtilityActionOutput.setDisplayName(MAX_UTIL_ACTION_OUT_DISP);

        actionTendenciesOutput = createRecordOutputPort(this, ACTION_TENDENCIES_OUT);
        actionTendenciesOutput.setDisplayName(ACTION_TENDENCIES_OUT_DISP);
    }

    @Override
    public void fire() throws IllegalActionException {
        super.fire();

        ArrayToken featureToActions = (ArrayToken) featureToActionInput.get(0);
        Map<String, List<Edge>> featureActionAssociations = EDGE_LIST_CONVERTER.convert(featureToActions)
                .stream().collect(groupingBy(Edge::getSource));

        RecordToken actions = (RecordToken) actionInput.get(0);
        Map<String, String> actionTypes = STRING_MAP_CONVERTER.convert(actions);

        ArrayToken actionsToGoals = (ArrayToken) actionToGoalInput.get(0);
        Map<String, List<Edge>> actionGoalTransitions = EDGE_LIST_CONVERTER.convert(actionsToGoals)
                .stream().collect(groupingBy(Edge::getSource));

        RecordToken goals = (RecordToken) goalInput.get(0);
        Map<String, Double> goalAmbitions = DOUBLE_MAP_CONVERTER.convert(goals);

        UtilityAlgo utility = new UtilityAlgo(featureActionAssociations, actionTypes, actionGoalTransitions, goalAmbitions);
        UtilityResult utilityResult = utility.calcluateUtility(featureActionAssociations.keySet());

        Map<String, Token> featureUtilities= utilityResult.getFeatureUtilitiesResult().entrySet().stream()
                .collect(toMap(Map.Entry::getKey, mapEntry -> createFuzzyWeightToken(mapEntry)));
        utilitiesOutput.send(0, new RecordToken(featureUtilities));

        Map<String, Token> featureActions = utilityResult.getFeatureActionResult().entrySet().stream()
                .collect(toMap(Map.Entry::getKey, mapEntry -> new StringToken(mapEntry.getValue())));
        maxUtilityActionOutput.send(0, new RecordToken(featureActions));

        Map<String, Token> actionTendencies = utilityResult.getActionTendenciesResult().entrySet().stream()
                .collect(toMap(Map.Entry::getKey, this::convertToRecord));
        actionTendenciesOutput.send(0, new RecordToken(actionTendencies));

        Map<String, Token> actionTransitions = utilityResult.getActionTransitionsResult().entrySet().stream()
                .collect(toMap(Map.Entry::getKey, this::createArrayTokenFromTransitions));
        transitionsOutput.send(0, new RecordToken(actionTransitions));
    }

    private FuzzyWeightToken createFuzzyWeightToken(Entry<String, FuzzyWeight> mapEntry) {
        try {
            return new FuzzyWeightToken(mapEntry.getValue());
        } catch (IllegalActionException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private Token convertToRecord(Map.Entry<String, Map<String, Double>> actionTendenciesEntry) {
        try {
            return DOUBLE_MAP_CONVERTER.convert(actionTendenciesEntry.getValue());
        } catch (IllegalActionException ex) {
            // This should never happen
            throw new RuntimeException(ex);
        }
    }

    private Token createArrayTokenFromTransitions(Map.Entry<String, List<ActionGoalTransition>> transitionEntry) {
        List<Token> list = transitionEntry.getValue().stream()
            .map(this::createObjectToken)
            .collect(toList());

        try {
            return new ArrayToken(list.toArray(new Token[0]));
        } catch (IllegalActionException e) {
            // This should never happen
            throw new RuntimeException(e);
        }
    }

    private Token createObjectToken(ActionGoalTransition trans) {
        try {
            return new ObjectToken(trans, ActionGoalTransition.class);
        } catch (IllegalActionException e) {
            // This should never happen
            throw new RuntimeException(e);
        }
    }
}
