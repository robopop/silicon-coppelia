package org.bitbucket.marrs.sico.actor.lib.input;

import static com.google.common.base.Preconditions.checkArgument;
import static org.bitbucket.marrs.sico.actor.lib.util.TokenConverter.DOUBLE_MAP_CONVERTER;
import static org.bitbucket.marrs.sico.actor.lib.util.TokenConverter.INT_MAP_CONVERTER;
import static ptolemy.data.MatrixToken.DO_COPY;

import java.util.List;
import java.util.Map;

import com.google.common.primitives.Doubles;

import ptolemy.data.DoubleMatrixToken;
import ptolemy.data.DoubleToken;
import ptolemy.data.RecordToken;
import ptolemy.kernel.util.IllegalActionException;

final class WeightParameters {
    private final Map<String, Double> similarityWeights;
    private final Map<String, Double> dissimilarityWeights;
    private final double satisfactionWeight;
    private final double idtWeight;
    private final List<Double> decisionWeightMatrix;
    private final Map<String, Integer> decisionRowMapping;
    private final List<Double> engagementWeightMatrix;
    private final Map<String, Integer> engagementRowMapping;
    private final List<Double> useIntentionsWeightMatrix;
    private final Map<String, Integer> useIntentionsRowMapping;

    WeightParameters(Map<String, Double> similarityWeights, Map<String, Double> dissimilarityWeights, double satisfactionWeight,
            double idtWeight, List<Double> decisionWeightMatrix, Map<String, Integer> decisionRowMapping,
            List<Double> engagementWeightMatrix, Map<String, Integer> engagementRowMapping,
            List<Double> useIntentionsWeightMatrix, Map<String, Integer> useIntentionsRowMapping) {
        this.similarityWeights = similarityWeights;
        this.dissimilarityWeights = dissimilarityWeights;
        this.satisfactionWeight = satisfactionWeight;
        this.idtWeight = idtWeight;
        this.decisionWeightMatrix = decisionWeightMatrix;
        this.decisionRowMapping = decisionRowMapping;
        this.engagementWeightMatrix = engagementWeightMatrix;
        this.engagementRowMapping = engagementRowMapping;
        this.useIntentionsWeightMatrix = useIntentionsWeightMatrix;
        this.useIntentionsRowMapping = useIntentionsRowMapping;
    }

    void validate() {
        checkArgumentNonNull(similarityWeights, "similarityWeights");
        checkArgumentNonNull(dissimilarityWeights, "dissimilarityWeights");
        checkArgumentNonNull(decisionWeightMatrix, "decisionWeightMatrix");
        checkArgumentNonNull(decisionRowMapping, "decisionRowMapping");
        checkArgumentNonNull(engagementWeightMatrix, "engagementWeightMatrix");
        checkArgumentNonNull(engagementRowMapping, "engagementRowMapping");
        checkArgumentNonNull(useIntentionsWeightMatrix, "useIntentionsWeightMatrix");
        checkArgumentNonNull(useIntentionsRowMapping, "useIntentionsRowMapping");
    }

    private void checkArgumentNonNull(Object ref, String message) {
        checkArgument(ref != null, message);
    }

    RecordToken getSimilarityWeights() throws IllegalActionException {
        return DOUBLE_MAP_CONVERTER.convert(similarityWeights);
    }

    RecordToken getDissimilarityWeights() throws IllegalActionException {
        return DOUBLE_MAP_CONVERTER.convert(dissimilarityWeights);
    }

    DoubleToken getSatisfactionWeight() {
        return new DoubleToken(satisfactionWeight);
    }

    DoubleToken getIdtWeight() {
        return new DoubleToken(idtWeight);
    }

    DoubleMatrixToken getDecisionWeightMatrix() throws IllegalActionException {
        int rows = decisionRowMapping.size();
        int columns = decisionWeightMatrix.size() / rows;

        return new DoubleMatrixToken(Doubles.toArray(decisionWeightMatrix), rows, columns, DO_COPY);
    }

    RecordToken getDecisionRowMapping() throws IllegalActionException {
        return INT_MAP_CONVERTER.convert(decisionRowMapping);
    }

    DoubleMatrixToken getEngagementWeightMatrix() throws IllegalActionException {
        int rows = engagementRowMapping.size();
        int columns = engagementWeightMatrix.size() / rows;

        return new DoubleMatrixToken(Doubles.toArray(engagementWeightMatrix), rows, columns, DO_COPY);
    }

    RecordToken getEngagementRowMapping() throws IllegalActionException {
        return INT_MAP_CONVERTER.convert(engagementRowMapping);
    }

    DoubleMatrixToken getUseIntentionsWeightMatrix() throws IllegalActionException {
        int rows = useIntentionsRowMapping.size();
        int columns = useIntentionsWeightMatrix.size() / rows;

        return new DoubleMatrixToken(Doubles.toArray(useIntentionsWeightMatrix), rows, columns, DO_COPY);
    }

    RecordToken getUseIntentionsRowMapping() throws IllegalActionException {
        return INT_MAP_CONVERTER.convert(useIntentionsRowMapping);
    }
}
