package org.bitbucket.marrs.sico.actor.lib.input;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.function.Function;

import ptolemy.data.type.BaseType;
import ptolemy.data.type.RecordType;
import ptolemy.data.type.Type;

public enum FeaturesPort {
	FEATURES("features", "Features", BaseType.RECORD),
    ETHICS("ethics", "Ethics", BaseType.RECORD),
    AFFORDANCES("affordances", "Affordances", BaseType.RECORD),
    AESTHETICS("aesthetics", "Aesthetics", BaseType.RECORD),
    EPISTEMICS("epistemics", "Epistemics", BaseType.RECORD);

	private static final String[] STRING_ARRAY = new String[0];
    private static final Type[] TYPE_ARRAY = new Type[0];

    public final String name;
    public final String inputName;
    public final String inputNameTotal;
    public final String inputDisplayName;
    public final String inputDisplayNameTotal;
    public final String outputName;
    public final String outputNameTotal;
    public final String outputDisplayName;
    public final String outputDisplayNameTotal;
    public final Type type;

    FeaturesPort(String fieldName, String displayName, Type type) {
        this.name = fieldName;
		this.type = type;
        this.inputName = fieldName + "Input";
        this.inputNameTotal = fieldName + "InputTotal";
        this.inputDisplayName = displayName;
        this.inputDisplayNameTotal = displayName + " total";
        this.outputName = fieldName + "Output";
        this.outputNameTotal = fieldName + "OutputTotal";
        this.outputDisplayName = displayName;
        this.outputDisplayNameTotal = displayName + " total";
    }
    
    public String getInputName() {
    	return inputName;
    }

    public static Type getRecordType() {
        return new RecordType(names().toArray(STRING_ARRAY), types().toArray(TYPE_ARRAY));
    }
    
    public static List<String> names() {
    	return toNameList(port -> port.name);
    }

    public static List<String> inputNames() {
    	return toNameList(port -> port.inputName);
    }
    
    public static List<String> outputNames() {
    	return toNameList(port -> port.outputName);
    }

    public static List<String> inputNamesTotal() {
        return toNameList(port -> port.inputNameTotal);
    }

    private static List<String> toNameList(Function<FeaturesPort, String> toNameFunction) {
        return asList(values()).stream()
                .map(toNameFunction)
                .collect(toList());
    }
    
    private static List<Type> types() {
    	return asList(values()).stream()
                .map(port -> port.type)
                .collect(toList());
    }
}