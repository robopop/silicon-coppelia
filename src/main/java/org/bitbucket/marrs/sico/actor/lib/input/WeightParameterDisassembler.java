package org.bitbucket.marrs.sico.actor.lib.input;

import ptolemy.actor.TypedIOPort;
import ptolemy.actor.lib.RecordDisassembler;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;

public final class WeightParameterDisassembler extends RecordDisassembler {
    public TypedIOPort simillarityWeightsOutput;
    public TypedIOPort dissimillarityWeightsOutput;
    public TypedIOPort satisfactionWeightOutput;
    public TypedIOPort decisionWeightMatrixOutput;
    public TypedIOPort decisionRowMappingOutput;
    public TypedIOPort idtWeightOutput;
    public TypedIOPort engagementWeightMatrixOutput;
    public TypedIOPort engagementRowMappingOutput;
    public TypedIOPort useIntentionsWeightMatrixOutput;
    public TypedIOPort useIntentionsRowMappingOutput;

    public WeightParameterDisassembler(CompositeEntity container, String name) throws NameDuplicationException, IllegalActionException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
    	simillarityWeightsOutput = (TypedIOPort) newPort(WeightParameterPort.SIMILARITY_WEIGHTS.name);
    	simillarityWeightsOutput.setDisplayName(WeightParameterPort.SIMILARITY_WEIGHTS.name);
    	simillarityWeightsOutput.setTypeEquals(WeightParameterPort.SIMILARITY_WEIGHTS.type);
    	simillarityWeightsOutput.setOutput(true);
    	dissimillarityWeightsOutput = (TypedIOPort) newPort(WeightParameterPort.DISSIMILARITY_WEIGHTS.name);
    	dissimillarityWeightsOutput.setDisplayName(WeightParameterPort.DISSIMILARITY_WEIGHTS.name);
    	dissimillarityWeightsOutput.setTypeEquals(WeightParameterPort.DISSIMILARITY_WEIGHTS.type);
    	dissimillarityWeightsOutput.setOutput(true);
    	satisfactionWeightOutput = (TypedIOPort) newPort(WeightParameterPort.SATISFACTION_WEIGHT.name);
    	satisfactionWeightOutput.setDisplayName(WeightParameterPort.SATISFACTION_WEIGHT.name);
    	satisfactionWeightOutput.setTypeEquals(WeightParameterPort.SATISFACTION_WEIGHT.type);
    	satisfactionWeightOutput.setOutput(true);
    	decisionWeightMatrixOutput = (TypedIOPort) newPort(WeightParameterPort.DECISION_WEIGHT_MATRIX.name);
    	decisionWeightMatrixOutput.setDisplayName(WeightParameterPort.DECISION_WEIGHT_MATRIX.name);
    	decisionWeightMatrixOutput.setTypeEquals(WeightParameterPort.DECISION_WEIGHT_MATRIX.type);
    	decisionWeightMatrixOutput.setOutput(true);
    	decisionRowMappingOutput = (TypedIOPort) newPort(WeightParameterPort.DECISION_ROW_MAPPING.name);
    	decisionRowMappingOutput.setDisplayName(WeightParameterPort.DECISION_ROW_MAPPING.name);
    	decisionRowMappingOutput.setTypeEquals(WeightParameterPort.DECISION_ROW_MAPPING.type);
    	decisionRowMappingOutput.setOutput(true);
    	idtWeightOutput = (TypedIOPort) newPort(WeightParameterPort.IDT_WEIGHT.name);
    	idtWeightOutput.setDisplayName(WeightParameterPort.IDT_WEIGHT.name);
    	idtWeightOutput.setTypeEquals(WeightParameterPort.IDT_WEIGHT.type);
    	idtWeightOutput.setOutput(true);
    	engagementWeightMatrixOutput = (TypedIOPort) newPort(WeightParameterPort.ENGAGMENT_WEIGHT_MATRIX.name);
    	engagementWeightMatrixOutput.setDisplayName(WeightParameterPort.ENGAGMENT_WEIGHT_MATRIX.name);
    	engagementWeightMatrixOutput.setTypeEquals(WeightParameterPort.ENGAGMENT_WEIGHT_MATRIX.type);
    	engagementWeightMatrixOutput.setOutput(true);
    	engagementRowMappingOutput = (TypedIOPort) newPort(WeightParameterPort.ENGAGEMENT_ROW_MAPPING.name);
    	engagementRowMappingOutput.setDisplayName(WeightParameterPort.ENGAGEMENT_ROW_MAPPING.name);
    	engagementRowMappingOutput.setTypeEquals(WeightParameterPort.ENGAGEMENT_ROW_MAPPING.type);
    	engagementRowMappingOutput.setOutput(true);
    	useIntentionsWeightMatrixOutput = (TypedIOPort) newPort(WeightParameterPort.USE_INTENTIONS_WEIGHT_MATRIX.name);
    	useIntentionsWeightMatrixOutput.setDisplayName(WeightParameterPort.USE_INTENTIONS_WEIGHT_MATRIX.name);
    	useIntentionsWeightMatrixOutput.setTypeEquals(WeightParameterPort.USE_INTENTIONS_WEIGHT_MATRIX.type);
    	useIntentionsWeightMatrixOutput.setOutput(true);
    	useIntentionsRowMappingOutput = (TypedIOPort) newPort(WeightParameterPort.USE_INTENTIONS_ROW_MAPPING.name);
    	useIntentionsRowMappingOutput.setDisplayName(WeightParameterPort.USE_INTENTIONS_ROW_MAPPING.name);
    	useIntentionsRowMappingOutput.setTypeEquals(WeightParameterPort.USE_INTENTIONS_ROW_MAPPING.type);
    	useIntentionsRowMappingOutput.setOutput(true);

    	input.setTypeAtMost(BaseType.RECORD);
    }
}
