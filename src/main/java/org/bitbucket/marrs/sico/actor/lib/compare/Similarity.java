package org.bitbucket.marrs.sico.actor.lib.compare;

import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordOutputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createTypedOutputPort;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;

import org.bitbucket.marrs.sico.actor.lib.base.DomainPort;
import org.bitbucket.marrs.sico.actor.lib.input.FeatureDisassembler;
import org.bitbucket.marrs.sico.actor.lib.input.FeaturesPort;
import org.bitbucket.marrs.sico.actor.lib.util.PortUtil;
import org.bitbucket.marrs.sico.data.type.FuzzyWeightType;

import ptolemy.actor.TypedCompositeActor;
import ptolemy.actor.TypedIOPort;
import ptolemy.actor.lib.Discard;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;

public final class Similarity extends TypedCompositeActor {

	private static final String FEATURES_IN = "featuresInput";
    private static final String FEATURES_IN_DISP = "Features";
    private static final String SIMILARITY_OUT = "similarityOutput";
    private static final String SIMILARITY_OUT_DISP = "Similarity";
    private static final String SIMILARITY_TOTAL_OUT = "similarityOutputTotal";
    private static final String SIMILARITY_TOTAL_OUT_DISP = "Similarity Total";

    private static final String FEATURE_SINK = "featureSink";
    private static final String FEATURE_SINK_DISP = "Discard";
    private static final String FEATURE_SELF_SINK = "featureSelfSink";
    private static final String FEATURE_SELF_SINK_DISP = "Discard Self";
    private static final String SELF_ENCODER_ACTOR = "selfEncoding";
    private static final String SELF_ENCODER_ACTOR_DISP = "Encoding Self";
    private static final String DISTANCE_ACTOR = "similarityDistance";
    private static final String DISTANCE_ACTOR_DISP = "Similarity Distance";

    private static final String FEATURES_SELF_INPUT = "featuresSelfInput";
    private static final String FEATURES_SELF_INPUT_DISP = "Features Self";
    private static final String SIMILARITY_WEIGHTS_INPUT = "similarityWeightsInput";
    private static final String SIMILARITY_WEIGHTS_INPUT_DISP = "Similarity Weights";
    private static final String DISSIMILARITY_WEIGHTS_INPUT = "dissimilarityWeightsInput";
    private static final String DISSIMILARITY_WEIGHTS_INPUT_DISP = "Dissimilarity Weights";

    /**
     * Input port for the features. The port is of type {@link BaseType#RECORD}
     * with values of type {@link BaseType#DOUBLE}.
     */
    public TypedIOPort featuresInput;

    /**
     * Input port for the feature values in the ethic domain. The port is of
     * type {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}.
     */
    public TypedIOPort ethicsInput;

    /**
     * Input port for the feature values in the affordances domain. The port is
     * of type {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}.
     */
    public TypedIOPort affordancesInput;

    /**
     * Input port for the feature values in the aesthetics domain. The port is
     * of type {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}.
     */
    public TypedIOPort aestheticsInput;

    /**
     * Input port for the feature values in the epistemics domain. The port is
     * of type {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}.
     */
    public TypedIOPort epistemicsInput;

    /**
     * Output port for the similarity values. The output is of type
     * {@link BaseType#RECORD} with values of type {@link FuzzyWeightType}. The
     * indicative values represents similarity, the counter-indicative values
     * represents dissimilarity.
     */
    public TypedIOPort similarityOutput;

    /**
     * Output port for the aggregate similarity value. The output is of type
     * {@link FuzzyWeightType}. The indicative value represents the total
     * similarity, the counter-indicative value represents total dissimilarity.
     */
    public TypedIOPort similarityOutputTotal;


    /**
     * Input port for feature - action associations. The input is of type
     * {@link BaseType#RECORD} where the keys are feature names and the values
     * are arrays of JSON strings that describe the associations of features
     * with actions. The JSON strings contain a JSON object describing the
     * feature in the property "concept1", a JSON object describing the action
     * in the property "concept2" and the truth value of the association in the
     * property "truthValue".
     */
    public TypedIOPort featuresSelfInput;

    /**
     * Input port for data about the actions. The input is of type
     * {@link BaseType#RECORD} where the keys are action names and the values
     * are JSON strings that hold data about the actions. The JSON strings
     * contain a JSON object describing the action in the property "concept",
     * the name of the data in the property "relation" and a string
     * representation of the value of the data in the property "value".
     */
    public TypedIOPort similarityWeightsInput;

    /**
     * Input port for action - goal associations. The input is of type
     * {@link BaseType#RECORD} where the keys are action names and the values
     * are arrays of JSON string tuples that describe the associations of
     * actions with the goals.
     * <p>
     * The first entry in the tuple is a JSON object that contains a JSON
     * object describing the action in the property "concept1", a JSON object
     * describing the goal in the property "concept2" and the truth value of
     * the association in the property "truthValue".
     * <p>
     * The second entry in the tuple is a JSON object that contains a JSON
     * object describing data related to the association. It contains a JSON
     * object describing the action in the property "concept1", a JSON object
     * describing the goal in the property "concept2", the name of the data in
     * the property "relation" and a string representation of the value of the
     * data in the property "value".
     */
    public TypedIOPort dissimilarityWeightsInput;

    public Discard featureSink;
    public Discard featureSelfSink;
    public FeatureDisassembler selfEncoding;
    public SimilarityDistance similarityDistance;

    public Similarity(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name);

        this.featuresInput = createRecordInputPort(this, FEATURES_IN);
        this.featuresInput.setDisplayName(FEATURES_IN_DISP);

        this.ethicsInput = createRecordInputPort(this, DomainPort.ETHICS.inputName);
        this.ethicsInput.setDisplayName(DomainPort.ETHICS.inputDisplayName);
        this.affordancesInput = createRecordInputPort(this, DomainPort.AFFORDANCES.inputName);
        this.affordancesInput.setDisplayName(DomainPort.AFFORDANCES.inputDisplayName);
        this.aestheticsInput = createRecordInputPort(this, DomainPort.AESTHETICS.inputName);
        this.aestheticsInput.setDisplayName(DomainPort.AESTHETICS.inputDisplayName);
        this.epistemicsInput = createRecordInputPort(this, DomainPort.EPISTEMICS.inputName);
        this.epistemicsInput.setDisplayName(DomainPort.EPISTEMICS.inputDisplayName);

        this.similarityOutput = createRecordOutputPort(this, SIMILARITY_OUT);
        this.similarityOutput.setDisplayName(SIMILARITY_OUT_DISP);
        this.similarityOutputTotal = createTypedOutputPort(this, SIMILARITY_TOTAL_OUT, FUZZY_WEIGHT);
        this.similarityOutputTotal.setDisplayName(SIMILARITY_TOTAL_OUT_DISP);

        this.featureSink = new Discard(this, FEATURE_SINK);
        this.featureSink.setDisplayName(FEATURE_SINK_DISP);
        this.featureSelfSink = new Discard(this, FEATURE_SELF_SINK);
        this.featureSelfSink.setDisplayName(FEATURE_SELF_SINK_DISP);

        this.selfEncoding = new FeatureDisassembler(this, SELF_ENCODER_ACTOR);
        this.selfEncoding.setDisplayName(SELF_ENCODER_ACTOR_DISP);
        this.similarityDistance = new SimilarityDistance(this, DISTANCE_ACTOR);
        this.similarityDistance.setDisplayName(DISTANCE_ACTOR_DISP);

        this.featuresSelfInput = PortUtil.createTypedInputPort(this, FEATURES_SELF_INPUT, FeaturesPort.getRecordType());
        this.featuresSelfInput.setDisplayName(FEATURES_SELF_INPUT_DISP);
        this.similarityWeightsInput = createRecordInputPort(this, SIMILARITY_WEIGHTS_INPUT);
        this.similarityWeightsInput.setDisplayName(SIMILARITY_WEIGHTS_INPUT_DISP);
        this.dissimilarityWeightsInput = createRecordInputPort(this, DISSIMILARITY_WEIGHTS_INPUT);
        this.dissimilarityWeightsInput.setDisplayName(DISSIMILARITY_WEIGHTS_INPUT_DISP);

        connect(featuresInput, featureSink.input);

        connect(featuresSelfInput, selfEncoding.input);
        connect(similarityWeightsInput, similarityDistance.simmilarityWeightsInput);
        connect(dissimilarityWeightsInput, similarityDistance.dissimmilarityWeightsInput);

        connect(selfEncoding.featuresOutput, featureSelfSink.input);
        connect(selfEncoding.ethicsOutput, similarityDistance.ethicsInputSelf);
        connect(selfEncoding.affordancesOutput, similarityDistance.affordancesInputSelf);
        connect(selfEncoding.aestheticsOutput, similarityDistance.aestheticsInputSelf);
        connect(selfEncoding.epistemicsOutput, similarityDistance.epistemicsInputSelf);

        connect(ethicsInput, similarityDistance.ethicsInput);
        connect(affordancesInput, similarityDistance.affordancesInput);
        connect(aestheticsInput, similarityDistance.aestheticsInput);
        connect(epistemicsInput, similarityDistance.epistemicsInput);

        connect(similarityDistance.getSimilarityOutput(), similarityOutput);
        connect(similarityDistance.getSimilarityOutputTotal(), similarityOutputTotal);
    }
}
