package org.bitbucket.marrs.sico.actor.lib.util;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.bitbucket.marrs.sico.data.type.EdgeType.EDGE;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.bitbucket.marrs.sico.data.Edge;
import org.bitbucket.marrs.sico.data.token.EdgeToken;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

import ptolemy.data.ArrayToken;
import ptolemy.data.DoubleToken;
import ptolemy.data.IntToken;
import ptolemy.data.RecordToken;
import ptolemy.data.StringToken;
import ptolemy.data.Token;
import ptolemy.kernel.util.IllegalActionException;

public interface TokenConverter<T> {

    T convert(Token token) throws IllegalActionException;

    Token convert(T value) throws IllegalActionException;

    static RecordTokenConverter<String> STRING_MAP_CONVERTER =
            new RecordTokenConverter<String>() {
                @Override
                protected String convertToken(Token token) throws IllegalActionException {
                    return StringToken.convert(token).stringValue();
                }

                @Override
                protected Token convertValue(String value) {
                    return new StringToken(value);
                }
            };

    static RecordTokenConverter<Integer> INT_MAP_CONVERTER =
            new RecordTokenConverter<Integer>() {
                @Override
                protected Integer convertToken(Token token) throws IllegalActionException {
                    return IntToken.convert(token).intValue();
                }

                @Override
                protected Token convertValue(Integer value) {
                    return new IntToken(value);
                }
            };

    static RecordTokenConverter<Double> DOUBLE_MAP_CONVERTER =
            new RecordTokenConverter<Double>() {
                @Override
                protected Double convertToken(Token token) throws IllegalActionException {
                    return DoubleToken.convert(token).doubleValue();
                }

                @Override
                protected Token convertValue(Double value) {
                    return new DoubleToken(value);
                }
            };

    static ArrayTokenConverter<Edge> EDGE_LIST_CONVERTER =
            new ArrayTokenConverter<Edge>() {
                @Override
                protected Edge convertToken(Token token) throws IllegalActionException {
                    return EDGE.convert(token).toEdge();
                }

                @Override
                protected Token convertValue(Edge value) {
                    return EdgeToken.fromEdge(value);
                }
            };

    static abstract class RecordTokenConverter<U> implements TokenConverter<Map<String, U>> {
        public Map<String, U> convert(Token record) throws IllegalActionException {
            try {
                return convert((RecordToken) record);
            } catch (Exception e) {
                throw new IllegalActionException("Cannot cast token to RecordToken: " + record.getType());
            }
        }

        public Map<String, U> convert(RecordToken record) throws IllegalActionException {
            Set<String> labelSet = record.labelSet();
            Builder<String, U> builder = ImmutableMap.builder();
            for (String label : labelSet) {
                builder.put(label, convertToken(record.get(label)));
            }

            return builder.build();
        }

        public RecordToken convert(Map<String, U> map) throws IllegalActionException {
            Map<String, Token> recordMap = map.entrySet().stream()
                    .collect(toMap(Entry::getKey, e -> convertValue(e.getValue())));

            return new RecordToken(recordMap);
        }

        protected abstract U convertToken(Token token) throws IllegalActionException;

        protected abstract Token convertValue(U value);
    }

    static abstract class ArrayTokenConverter<U> implements TokenConverter<List<U>> {
        private static final Token[] TOKEN_ARRAY = new Token[0];

        public List<U> convert(Token array) throws IllegalActionException {
            try {
                return convert((ArrayToken) array);
            } catch (Exception e) {
                throw new IllegalActionException("Cannot cast token to RecordToken: " + array.getType());
            }
        }

        public List<U> convert(ArrayToken array) throws IllegalActionException {
            ImmutableList.Builder<U> builder = ImmutableList.builder();
            for (Token token : asList(array.arrayValue())) {
                builder.add(convertToken(token));
            }

            return builder.build();
        }

        public ArrayToken convert(List<U> list) throws IllegalActionException {
            List<Token> tokenList = list.stream()
                .map(this::convertValue)
                .collect(toList());

            return new ArrayToken(tokenList.toArray(TOKEN_ARRAY));
        }

        protected abstract U convertToken(Token token) throws IllegalActionException;

        protected abstract Token convertValue(U value);
    }

}
