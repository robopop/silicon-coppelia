package org.bitbucket.marrs.sico.actor.lib.respond;

import static java.lang.Double.compare;
import static java.util.stream.Collectors.toMap;
import static org.bitbucket.marrs.sico.actor.lib.util.ParameterUtil.createStringParameter;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordOutputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createTypedInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createTypedOutputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.TokenConverter.INT_MAP_CONVERTER;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.bitbucket.marrs.sico.actor.lib.base.LinearModel;
import org.bitbucket.marrs.sico.data.token.FuzzyWeightToken;

import com.google.gson.JsonSyntaxException;

import ptolemy.actor.NoTokenException;
import ptolemy.actor.TypedAtomicActor;
import ptolemy.actor.TypedIOPort;
import ptolemy.data.DoubleMatrixToken;
import ptolemy.data.DoubleToken;
import ptolemy.data.RecordToken;
import ptolemy.data.StringToken;
import ptolemy.data.Token;
import ptolemy.data.expr.Parameter;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

/**
 * Chooses an action based on the expected satisfaction of features.
 * <p>
 * From the satisfaction input the feature with the maximum satisfaction is
 * selected and the related action is retrieved from an externally provided
 * action-to-feature map.
 * <p>
 * The result is the name of the selected action.
 */
public final class MaxSatisfactionDecision extends TypedAtomicActor {
    private static final String SATISFACTION_IN = "satisfactionInput";
    private static final String SATISFACTION_IN_DISP = "Satisfaction";
    private static final String ACTION_IN = "actionInput";
    private static final String ACTION_IN_DISP = "Feature actions";
    private static final String ACTION_TENDENCIES_IN = "actionTendenciesInput";
    private static final String ACTION_TENDENCIES_IN_DISP = "Action Tendencies";
    private static final String ENGAGEMENT_IN = "engagementInput";
    private static final String ENGAGEMENT_IN_DISP = "Engagement";

    private static final String WEIGHTS_IN = "weightsInput";
    private static final String WEIGHTS_IN_DISP = "Weights";
    private static final String ROW_MAPPING_IN = "rowMappingInput";
    private static final String ROW_MAPPING_IN_DISP = "Row mapping";

    private static final String ACTION_OUT = "actionOutput";
    private static final String ACTION_OUT_DISP = "Action";
    private static final String FEATURE_SAT_OUT = "featureSatisfactionOutput";
    private static final String FEATURE_SAT_OUT_DISP = "Feature Satisfaction";

    private static final String POS = "positive";
    private static final String NEG = "negative";
    private static final String AVOID = "avoid";
    private static final String CHANGE = "change";
    private static final String NIL = "nil";

    private static final String POSTFIX = "Param";

    /**
     * Parameter defining the key of the parameter for indicative affordances
     * value in the {@link LinearModel#rowMappingInput}.
     */
    public Parameter posParam;

    /**
     * Parameter defining the key of the parameter for counter-indicative
     * affordances value in the {@link LinearModel#rowMappingInput}.
     */
    public Parameter negParam;

    /**
     * Parameter defining the key of the parameter for indicative aesthetics
     * value in the {@link LinearModel#rowMappingInput}.
     */
    public Parameter avoidParam;

    /**
     * Parameter defining the key of the parameter for counter-indicative
     * aesthetics value in the {@link LinearModel#rowMappingInput}.
     */
    public Parameter changeParam;

    /**
     * Parameter defining the key of the parameter for counter-indicative
     * aesthetics value in the {@link LinearModel#rowMappingInput}.
     */
    public Parameter nilParam;

    /**
     * Input port for the satisfaction values of features. The input is of type
     * {@link BaseType#RECORD} with values of type {@link BaseType#DOUBLE}.
     */
    public TypedIOPort satisfactionInput;

    /**
     * Input port for the actions related to a feature. The input is of type
     * {@link BaseType#RECORD} and maps feature names to action names, i.e. keys
     * and values are of type {@link BaseType#STRING}.
     */
    public TypedIOPort actionInput;

    /**
     * Input port for actions tendencies for the features. The input
     * is of type {@link BaseType#RECORD}.
     */
    public TypedIOPort actionTendenciesInput;

    /**
     * Input port for actions tendencies for the features. The input
     * is of type {@link BaseType#RECORD}.
     */
    public TypedIOPort engagementInput;

    public TypedIOPort weightsInput;
    public TypedIOPort rowMappingInput;

    /**
     * Output port for selected action. The output is of type
     * {@link BaseType#STRING}.
     */
    public TypedIOPort actionOutput;

    public TypedIOPort featureSatisfactionOutput;

    public MaxSatisfactionDecision(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init();
    }

    public MaxSatisfactionDecision(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        this.posParam = createStringParameter(this, POS + POSTFIX, POS);
        this.negParam = createStringParameter(this, NEG+ POSTFIX, NEG);
        this.avoidParam = createStringParameter(this, AVOID + POSTFIX, AVOID);
        this.changeParam = createStringParameter(this, CHANGE + POSTFIX, CHANGE);
        this.nilParam = createStringParameter(this, NIL + POSTFIX, NIL);

        satisfactionInput = createRecordInputPort(this, SATISFACTION_IN);
        satisfactionInput.setDisplayName(SATISFACTION_IN_DISP);
        engagementInput = createRecordInputPort(this, ENGAGEMENT_IN);
        engagementInput.setDisplayName(ENGAGEMENT_IN_DISP);
        actionInput = createRecordInputPort(this, ACTION_IN);
        actionInput.setDisplayName(ACTION_IN_DISP);
        actionTendenciesInput = createRecordInputPort(this, ACTION_TENDENCIES_IN);
        actionTendenciesInput.setDisplayName(ACTION_TENDENCIES_IN_DISP);

        weightsInput = createTypedInputPort(this, WEIGHTS_IN, BaseType.DOUBLE_MATRIX);
        weightsInput.setDisplayName(WEIGHTS_IN_DISP);
        rowMappingInput = createRecordInputPort(this, ROW_MAPPING_IN);
        rowMappingInput.setDisplayName(ROW_MAPPING_IN_DISP);

        actionOutput = createTypedOutputPort(this, ACTION_OUT, BaseType.STRING);
        actionOutput.setDisplayName(ACTION_OUT_DISP);
        featureSatisfactionOutput = createRecordOutputPort(this, FEATURE_SAT_OUT);
        featureSatisfactionOutput.setDisplayName(FEATURE_SAT_OUT_DISP);
    }

    @Override
    public void fire() throws IllegalActionException {
        try {
            doFire();
        } catch (WrappedPtolemyException e) {
            throw (IllegalActionException) e.getCause();
        }
    }

    private void doFire() throws IllegalActionException {
        super.fire();

        Map<String, Integer> rowMapping = INT_MAP_CONVERTER.convert((RecordToken) rowMappingInput.get(0));

        DoubleMatrixToken weightMatrixToken = DoubleMatrixToken.convert(weightsInput.get(0));
        RealMatrix weightMatrix = new Array2DRowRealMatrix(weightMatrixToken.doubleMatrix(), false);

        checkDimensions(weightMatrix, rowMapping);

        RecordToken satisfactions = (RecordToken) satisfactionInput.get(0);

        Optional<String> feature = satisfactions.labelSet().parallelStream()
                .max((left, right) -> compareValues(satisfactions, left, right));

        Optional<String> maxSatisfactionAction = feature
                .map(feat -> actionSatisfaction(feat, weightMatrix, rowMapping));

        actionOutput.send(0, maxSatisfactionAction.map(StringToken::new).orElse(StringToken.NIL));
    }

    private int compareValues(RecordToken satisfactions, String left, String right) {
        double leftValue = getValue(satisfactions, left);
        double rightValue = getValue(satisfactions, right);

        return compare(leftValue, rightValue);
    }

    private double getValue(RecordToken satisfactions, String label) {
        try {
            return DoubleToken.convert(satisfactions.get(label)).doubleValue();
        } catch (IllegalActionException e) {
            throw new WrappedPtolemyException(e);
        }
    }

    private String actionSatisfaction(String feature, RealMatrix weightMatrix, Map<String, Integer> rowMapping) {
        try {
            FuzzyWeightToken engagement = FUZZY_WEIGHT.convert(((RecordToken) engagementInput.get(0)).get(feature));
            double involvement = engagement.getPositiveWeight();
            double distance = engagement.getNegativeWeight();

            RecordToken actionsMap = (RecordToken) actionInput.get(0);
            RecordToken actionTendencies = getActionTendencies(feature);

            RealVector featureVector = new ArrayRealVector(rowMapping.size());
            Map<String, String> actionsByType = new HashMap<>();
            for (String action : actionTendencies.labelSet()) {
                Double actionTendency = DoubleToken.convert(actionTendencies.get(action)).doubleValue();
                String actionType = parseActionType(actionsMap.get(action));
                actionsByType.put(actionType, action);

                int index = rowMapping.get(actionType);

                RealVector input = new ArrayRealVector(new double[] {
                        actionType.equals("positive") || actionType.equals("change") ? involvement : 1 - involvement,
                        actionType.equals("positive") ? 1 - distance : distance,
                        actionTendency
                    });
                double value = weightMatrix.getRowVector(index).dotProduct(input);

                featureVector.setEntry(index, value);
            }

            Optional<String> tend = rowMapping.entrySet().stream()
                    .filter(e -> e.getValue().intValue() == featureVector.getMaxIndex())
                    .findFirst()
                    .map(Entry::getKey);


            featureSatisfactionOutput.send(0, new RecordToken(rowMapping.entrySet().stream()
                    .collect(toMap(Entry::getKey, e -> new DoubleToken(featureVector.getEntry(e.getValue()))))));

            return tend.map(actionsByType::get).orElse("nil");
        } catch (NoTokenException | IllegalActionException e) {
            throw new RuntimeException(e);
        }
    }

    private RecordToken getActionTendencies(String feature) {
        try {
            RecordToken actionTendenciesMap = (RecordToken) actionTendenciesInput.get(0);

            return (RecordToken) actionTendenciesMap.get(feature);
        } catch (IllegalActionException e) {
            throw new WrappedPtolemyException(e);
        }
    }

    private String parseActionType(Token token) {
        try {
            return StringToken.convert(token).stringValue();
        } catch (JsonSyntaxException | IllegalActionException e) {
            throw new RuntimeException(e);
        }
    }

    private void checkDimensions(RealMatrix weightMatrix, Map<String, Integer> rowMapping) throws IllegalActionException {
        if (weightMatrix.getColumnDimension() != 3) {
            throw new IllegalActionException(this, "Weight matrix must have 3 columns, was: " + weightMatrix.getColumnDimension());
        }

        if (weightMatrix.getRowDimension() != 5) {
            throw new IllegalActionException(this, "Weight matrix must have 5 rows, was: " + weightMatrix.getRowDimension());
        }

        if (rowMapping.size() != weightMatrix.getRowDimension()) {
            throw new IllegalActionException(this, "Row mapping does not match matrix row dimensions [" + weightMatrix.getRowDimension() + "]: " + rowMapping);
        }
    }

    private static class WrappedPtolemyException extends RuntimeException {
        private static final long serialVersionUID = 1L;

        // Wrap checked exception
        WrappedPtolemyException(IllegalActionException e) {
            super(e);
        }
    }
}
