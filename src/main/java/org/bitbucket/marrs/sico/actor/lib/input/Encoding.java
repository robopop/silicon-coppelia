package org.bitbucket.marrs.sico.actor.lib.input;

import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;

import org.bitbucket.marrs.sico.actor.lib.base.DomainPort;
import org.bitbucket.marrs.sico.data.type.FuzzyWeightType;

import ptolemy.actor.TypedIOPort;
import ptolemy.actor.lib.RecordDisassembler;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;

/**
 * Actor for the encoding of the features in the appraisal domains.
 * <p>
 * The actor disassembles the input token, which must contain records for the
 * keys {@code aesthetics}, {@code ethics}, {@code epistemics} and
 * {@code affordances}.
 */
public final class Encoding extends RecordDisassembler {


    /**
     * Output for the encoded ethics values for of features. The output is of
     * type {@link BaseType#RECORD}, where the values are of type
     * {@link FuzzyWeightType}. The indicative/counter-indicative value of an
     * output value is associated with the attribute good/bad.
     */
    public TypedIOPort ethicsOutput;

    /**
     * Output for the encoded affordances values of the features. The output is
     * of type {@link BaseType#RECORD}, where the values are of type
     * {@link FuzzyWeightType}. The indicative/counter-indicative value of an
     * output value is associated with the attribute aid/obstacle.
     */
    public TypedIOPort affordancesOutput;

    /**
     * Output for the encoded aesthetics values of the features. The output is
     * of type {@link BaseType#RECORD}, where the values are of type
     * {@link FuzzyWeightType}. The indicative/counter-indicative value of an
     * output value is associated with the attribute beautiful/ugly.
     */
    public TypedIOPort aestheticsOutput;

    /**
     * Output for the encoded epistemics values of the features. The output is
     * of type {@link BaseType#RECORD}, where the values are of type
     * {@link FuzzyWeightType}. The indicative/counter-indicative value of an
     * output value is associated with the attribute realistic/unrealistic.
     */
    public TypedIOPort epistemicsOutput;

    public Encoding(CompositeEntity container, String name) throws NameDuplicationException, IllegalActionException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
    	input.setTypeAtMost(BaseType.RECORD);

    	epistemicsOutput = (TypedIOPort) newPort(DomainPort.EPISTEMICS.outputName);
        epistemicsOutput.setDisplayName(DomainPort.EPISTEMICS.outputDisplayName);
        epistemicsOutput.setTypeEquals(FUZZY_WEIGHT);
        epistemicsOutput.setOutput(true);
        aestheticsOutput = (TypedIOPort) newPort(DomainPort.AESTHETICS.outputName);
        aestheticsOutput.setDisplayName(DomainPort.AESTHETICS.outputDisplayName);
        aestheticsOutput.setTypeEquals(FUZZY_WEIGHT);
        aestheticsOutput.setOutput(true);
        affordancesOutput = (TypedIOPort) newPort(DomainPort.AFFORDANCES.outputName);
        affordancesOutput.setDisplayName(DomainPort.AFFORDANCES.outputDisplayName);
        affordancesOutput.setTypeEquals(FUZZY_WEIGHT);
        affordancesOutput.setOutput(true);
        ethicsOutput = (TypedIOPort) newPort(DomainPort.ETHICS.outputName);
        ethicsOutput.setDisplayName(DomainPort.ETHICS.outputDisplayName);
        ethicsOutput.setTypeEquals(FUZZY_WEIGHT);
        ethicsOutput.setOutput(true);
    }
}
