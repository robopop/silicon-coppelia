package org.bitbucket.marrs.sico.actor.lib.input;

import ptolemy.actor.TypedIOPort;
import ptolemy.actor.lib.RecordDisassembler;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;

public final class BeliefDisassembler extends RecordDisassembler {
    public TypedIOPort featuresSelfOutput;
    public TypedIOPort featureToActionsOutput;
    public TypedIOPort actionTypesOutput;
    public TypedIOPort actionToGoalsOutput;
    public TypedIOPort goalAmbitionsOutput;

    public BeliefDisassembler(CompositeEntity container, String name) throws NameDuplicationException, IllegalActionException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
    	featuresSelfOutput = (TypedIOPort) newPort(BeliefPort.FEATURES_SELF.name);
    	featuresSelfOutput.setDisplayName(BeliefPort.FEATURES_SELF.name);
    	featuresSelfOutput.setTypeEquals(BeliefPort.FEATURES_SELF.type);
    	featuresSelfOutput.setOutput(true);
    	featureToActionsOutput = (TypedIOPort) newPort(BeliefPort.FEATURE_TO_ACTIONS.name);
    	featureToActionsOutput.setDisplayName(BeliefPort.FEATURE_TO_ACTIONS.name);
    	featureToActionsOutput.setTypeEquals(BeliefPort.FEATURE_TO_ACTIONS.type);
    	featureToActionsOutput.setOutput(true);
    	actionTypesOutput = (TypedIOPort) newPort(BeliefPort.ACTION_TYPES.name);
    	actionTypesOutput.setDisplayName(BeliefPort.ACTION_TYPES.name);
    	actionTypesOutput.setTypeEquals(BeliefPort.ACTION_TYPES.type);
    	actionTypesOutput.setOutput(true);
    	actionToGoalsOutput = (TypedIOPort) newPort(BeliefPort.ACTION_TO_GOALS.name);
    	actionToGoalsOutput.setDisplayName(BeliefPort.ACTION_TO_GOALS.name);
    	actionToGoalsOutput.setTypeEquals(BeliefPort.ACTION_TO_GOALS.type);
    	actionToGoalsOutput.setOutput(true);
    	goalAmbitionsOutput = (TypedIOPort) newPort(BeliefPort.GOAL_AMBITIONS.name);
    	goalAmbitionsOutput.setDisplayName(BeliefPort.GOAL_AMBITIONS.name);
    	goalAmbitionsOutput.setTypeEquals(BeliefPort.GOAL_AMBITIONS.type);
    	goalAmbitionsOutput.setOutput(true);

    	input.setTypeAtMost(BaseType.RECORD);
    }
}
