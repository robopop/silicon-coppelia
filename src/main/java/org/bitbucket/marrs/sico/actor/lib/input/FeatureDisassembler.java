package org.bitbucket.marrs.sico.actor.lib.input;

import ptolemy.actor.TypedIOPort;
import ptolemy.actor.lib.RecordDisassembler;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;

public final class FeatureDisassembler extends RecordDisassembler {
    public TypedIOPort aestheticsOutput;
    public TypedIOPort ethicsOutput;
    public TypedIOPort epistemicsOutput;
    public TypedIOPort affordancesOutput;
    public TypedIOPort featuresOutput;

    public FeatureDisassembler(CompositeEntity container, String name) throws NameDuplicationException, IllegalActionException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
    	aestheticsOutput = (TypedIOPort) newPort(FeaturesPort.AESTHETICS.name);
    	aestheticsOutput.setDisplayName(FeaturesPort.AESTHETICS.name);
    	aestheticsOutput.setTypeEquals(FeaturesPort.AESTHETICS.type);
    	aestheticsOutput.setOutput(true);
    	ethicsOutput = (TypedIOPort) newPort(FeaturesPort.ETHICS.name);
    	ethicsOutput.setDisplayName(FeaturesPort.ETHICS.name);
    	ethicsOutput.setTypeEquals(FeaturesPort.ETHICS.type);
    	ethicsOutput.setOutput(true);
    	epistemicsOutput = (TypedIOPort) newPort(FeaturesPort.EPISTEMICS.name);
    	epistemicsOutput.setDisplayName(FeaturesPort.EPISTEMICS.name);
    	epistemicsOutput.setTypeEquals(FeaturesPort.EPISTEMICS.type);
    	epistemicsOutput.setOutput(true);
    	affordancesOutput = (TypedIOPort) newPort(FeaturesPort.AFFORDANCES.name);
    	affordancesOutput.setDisplayName(FeaturesPort.AFFORDANCES.name);
    	affordancesOutput.setTypeEquals(FeaturesPort.AFFORDANCES.type);
    	affordancesOutput.setOutput(true);
    	featuresOutput = (TypedIOPort) newPort(FeaturesPort.FEATURES.name);
    	featuresOutput.setDisplayName(FeaturesPort.FEATURES.name);
    	featuresOutput.setTypeEquals(FeaturesPort.FEATURES.type);
    	featuresOutput.setOutput(true);

    	input.setTypeAtMost(BaseType.RECORD);
    }
}
