package org.bitbucket.marrs.sico.actor.lib.util;

import static java.util.stream.Collectors.toList;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.NEGATIVE;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.POSITIVE;

import java.util.Collection;
import java.util.List;
import java.util.stream.DoubleStream;

import org.bitbucket.marrs.sico.data.token.FuzzyWeightToken;

import ptolemy.data.DoubleToken;
import ptolemy.data.RecordToken;
import ptolemy.kernel.util.IllegalActionException;

public final class FuzzyWeightCalculator {
    
    public static FuzzyWeightToken calculateAverage(RecordToken fuzzyWeightsRecord) throws IllegalActionException {
        return calculateAverage(toFuzzyWeightTokens(fuzzyWeightsRecord));
    }

    public static FuzzyWeightToken calculateAverage(Collection<FuzzyWeightToken> fuzzyWeights) throws IllegalActionException {
        double positiveAvg = calculateAverage(fuzzyWeights, POSITIVE);
        double negativeAvg = calculateAverage(fuzzyWeights, NEGATIVE);

        return new FuzzyWeightToken(positiveAvg, negativeAvg);
    }

    public static double calculateAverage(Collection<FuzzyWeightToken> fuzzyWeights, String field) {
        return toFieldValueStream(fuzzyWeights, field)
                .average()
                .orElseThrow(() -> new RuntimeException("FuzzyWeightsToken must have field " + field));
    }

    public static FuzzyWeightToken calculateMax(RecordToken fuzzyWeightsRecord) throws IllegalActionException {
        return calculateMax(toFuzzyWeightTokens(fuzzyWeightsRecord));
    }

    public static FuzzyWeightToken calculateMax(Collection<FuzzyWeightToken> fuzzyWeights) throws IllegalActionException {
        double positiveMax = calculateMax(fuzzyWeights, POSITIVE);
        double negativeMax = calculateMax(fuzzyWeights, NEGATIVE);

        return new FuzzyWeightToken(positiveMax, negativeMax);
    }

    public static double calculateMax(Collection<FuzzyWeightToken> fuzzyWeights, String field) {
        return toFieldValueStream(fuzzyWeights, field)
                .max()
                .orElseThrow(() -> new RuntimeException("FuzzyWeightsToken must have field " + field));
    }

    private static List<FuzzyWeightToken> toFuzzyWeightTokens(RecordToken fuzzyWeightsRecord) {
        return fuzzyWeightsRecord.labelSet().stream()
                .map(fuzzyWeightsRecord::get)
                .map(FuzzyWeightToken.class::cast)
                .collect(toList());
    }

    private static DoubleStream toFieldValueStream(Collection<FuzzyWeightToken> fuzzyWeights, String field) {
        return fuzzyWeights.stream()
                .map(token -> token.get(field))
                .map(DoubleToken.class::cast)
                .mapToDouble(DoubleToken::doubleValue);
    }
}
