package org.bitbucket.marrs.sico.actor.lib.util;

import static java.util.stream.Collectors.averagingDouble;

import java.util.Collection;

import ptolemy.data.DoubleToken;

public final class DoubleWeightCalculator {

    public static DoubleToken calulateAverage(Collection<DoubleToken> tokens) {
        double average = tokens.parallelStream()
                .collect(averagingDouble(t -> t.doubleValue()));

        return new DoubleToken(average);
    }
}
