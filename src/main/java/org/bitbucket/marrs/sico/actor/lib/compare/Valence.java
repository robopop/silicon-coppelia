package org.bitbucket.marrs.sico.actor.lib.compare;

import static java.util.stream.Collectors.groupingBy;
import static org.bitbucket.marrs.sico.actor.lib.base.DomainPort.AFFORDANCES;
import static org.bitbucket.marrs.sico.actor.lib.util.FuzzyWeightCalculator.calculateMax;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createArrayInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordInputPort;
import static org.bitbucket.marrs.sico.actor.lib.util.TokenConverter.DOUBLE_MAP_CONVERTER;
import static org.bitbucket.marrs.sico.actor.lib.util.TokenConverter.EDGE_LIST_CONVERTER;
import static org.bitbucket.marrs.sico.actor.lib.util.TokenConverter.STRING_MAP_CONVERTER;
import static org.bitbucket.marrs.sico.data.type.EdgeType.EDGE;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.bitbucket.marrs.sico.actor.lib.base.MultiRecordTransformation;
import org.bitbucket.marrs.sico.algo.compare.ValenceAlgo;
import org.bitbucket.marrs.sico.data.Edge;
import org.bitbucket.marrs.sico.data.FuzzyWeight;
import org.bitbucket.marrs.sico.data.token.FuzzyWeightToken;
import org.bitbucket.marrs.sico.data.type.FuzzyWeightType;

import com.google.common.collect.ImmutableList;

import ptolemy.actor.TypedIOPort;
import ptolemy.data.ArrayToken;
import ptolemy.data.RecordToken;
import ptolemy.data.Token;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

public class Valence extends MultiRecordTransformation<FuzzyWeightToken> {
    private static final String FEATURES_IN = "features";
    private static final String FEATURES_IN_DISP = "Features";
    private static final String FEATURE_TO_ACTION_IN = "featureToActionInput";
    private static final String FEATURE_TO_ACTION_IN_DISP = "Feature - Actions";
    private static final String ACTION_IN = "actionInput";
    private static final String ACTION_IN_DISP = "Actions";
    private static final String ACTION_TO_GOAL_IN = "actionToGoalInput";
    private static final String ACTION_TO_GOAL_IN_DISP = "Action - Goals";
    private static final String GOAL_IN = "goalInput";
    private static final String GOAL_IN_DISP = "Goals";

    private static final List<String> INPUTS = ImmutableList.of(FEATURES_IN);

    /**
     * Input port for the encoded values of the features in the affordances
     * domain. The input port is of type {@link BaseType#RECORD} with values of
     * type {@link FuzzyWeightType} containing the encoded weights of the
     * affordances domain.
     */
    public TypedIOPort affordancesInput;

    /**
     * Input port for feature - action associations. The port is of type
     * {@link BaseType#RECORD} where the keys are feature names and the values
     * are arrays of JSON strings that describe the associations of features
     * with actions. The JSON strings contain a JSON object describing the
     * feature in the property "concept1", a JSON object describing the action
     * in the property "concept2" and the truth value of the association in the
     * property "truthValue".
     */
    public TypedIOPort featuresInput;

    /**
     * Input port for feature - action associations. The port is of type
     * {@link BaseType#RECORD} where the keys are feature names and the values
     * are arrays of JSON strings that describe the associations of features
     * with actions. The JSON strings contain a JSON object describing the
     * feature in the property "concept1", a JSON object describing the action
     * in the property "concept2" and the truth value of the association in the
     * property "truthValue".
     */
    public TypedIOPort featureToActionInput;

    /**
     * Input port for data about the actions. The port is of type
     * {@link BaseType#RECORD} where the keys are action names and the values
     * are JSON strings that hold data about the actions. The JSON strings
     * contain a JSON object describing the action in the property "concept",
     * the name of the data in the property "relation" and a string
     * representation of the value of the data in the property "value".
     */
    public TypedIOPort actionInput;

    /**
     * Input port for action - goal associations. The port is of type
     * {@link BaseType#RECORD} where the keys are action names and the values
     * are arrays of JSON string tuples that describe the associations of
     * actions with the goals.
     * <p>
     * The first entry in the tuple is a JSON object that contains a JSON
     * object describing the action in the property "concept1", a JSON object
     * describing the goal in the property "concept2" and the truth value of
     * the association in the property "truthValue".
     * <p>
     * The second entry in the tuple is a JSON object that contains a JSON
     * object describing data related to the association. It contains a JSON
     * object describing the action in the property "concept1", a JSON object
     * describing the goal in the property "concept2", the name of the data in
     * the property "relation" and a string representation of the value of the
     * data in the property "value".
     */
    public TypedIOPort actionToGoalInput;

    /**
     * Input port for data about the goals. The port is of type
     * {@link BaseType#RECORD} where the keys are goal names and the values
     * are arrays of JSON strings that hold data about the goals. The JSON
     * strings contain a JSON object describing the goal in the property
     * "concept", the name of the data in the property "relation" and a string
     * representation of the value of the data in the property "value".
     */
    public TypedIOPort goalInput;

    public Valence(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace, INPUTS, FUZZY_WEIGHT, BaseType.DOUBLE);
        init();
    }

    public Valence(CompositeEntity container, String name) throws IllegalActionException, NameDuplicationException {
        super(container, name, INPUTS, FUZZY_WEIGHT, BaseType.DOUBLE);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        featuresInput = getInput(0);
        featuresInput.setDisplayName(FEATURES_IN_DISP);

        affordancesInput = createRecordInputPort(this, AFFORDANCES.inputName);
        affordancesInput.setDisplayName(AFFORDANCES.inputDisplayName);

        featureToActionInput = createArrayInputPort(this, FEATURE_TO_ACTION_IN, EDGE);
        featureToActionInput.setDisplayName(FEATURE_TO_ACTION_IN_DISP);

        actionInput = createRecordInputPort(this, ACTION_IN);
        actionInput.setDisplayName(ACTION_IN_DISP);

        actionToGoalInput = createArrayInputPort(this, ACTION_TO_GOAL_IN, EDGE);
        actionToGoalInput.setDisplayName(ACTION_TO_GOAL_IN_DISP);

        goalInput = createRecordInputPort(this, GOAL_IN);
        goalInput.setDisplayName(GOAL_IN_DISP);

    }

    @Override
    protected Transformation<Token, FuzzyWeightToken> getTransformation() throws IllegalActionException {
        ArrayToken featureToActions = (ArrayToken) featureToActionInput.get(0);
        Map<String, List<Edge>> featureActionAssociations = EDGE_LIST_CONVERTER.convert(featureToActions)
                .stream().collect(groupingBy(Edge::getSource));

        RecordToken actions = (RecordToken) actionInput.get(0);
        Map<String, String> actionTypes = STRING_MAP_CONVERTER.convert(actions);

        ArrayToken actionsToGoals = (ArrayToken) actionToGoalInput.get(0);
        Map<String, List<Edge>> actionGoalTransitions = EDGE_LIST_CONVERTER.convert(actionsToGoals)
                .stream().collect(groupingBy(Edge::getSource));

        RecordToken goals = (RecordToken) goalInput.get(0);
        Map<String, Double> goalAmbitions = DOUBLE_MAP_CONVERTER.convert(goals);

        RecordToken affordances = (RecordToken) affordancesInput.get(0);

        ValenceAlgo valenceAlgo = new ValenceAlgo(featureActionAssociations, actionTypes, actionGoalTransitions, goalAmbitions);

        return (feature, args) -> {
                FuzzyWeight affordanceWeight = FUZZY_WEIGHT.convert(affordances.get(feature)).toFuzzyWeight();

                FuzzyWeight valence = valenceAlgo.calculateValence(feature, affordanceWeight);

                return new FuzzyWeightToken(valence);
        };
    }

    @Override
    protected Token aggregateOutput(Collection<FuzzyWeightToken> outputs) throws IllegalActionException {
        return calculateMax(outputs);
    }

}
