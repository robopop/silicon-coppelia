package org.bitbucket.marrs.sico.actor.lib.input;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.stream.Collectors.toSet;
import static org.bitbucket.marrs.sico.actor.lib.util.TokenConverter.DOUBLE_MAP_CONVERTER;
import static org.bitbucket.marrs.sico.actor.lib.util.TokenConverter.EDGE_LIST_CONVERTER;
import static org.bitbucket.marrs.sico.actor.lib.util.TokenConverter.STRING_MAP_CONVERTER;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bitbucket.marrs.sico.data.Edge;

import ptolemy.data.ArrayToken;
import ptolemy.data.RecordToken;
import ptolemy.kernel.util.IllegalActionException;

final class Beliefs {
    private final Map<String, String> actionTypes;
    private final Map<String, Double> goalAmbitions;
    private final List<Edge> featureToActions;
    private final List<Edge> actionToGoals;
    private final List<Feature> featuresSelf;

    Beliefs(Map<String, String> actionTypes,
            Map<String, Double> goalAmbitions,
            List<Edge> featureToActions,
            List<Edge> actionToGoals,
            List<Feature> featuresSelf) {
        this.actionTypes = actionTypes;
        this.goalAmbitions = goalAmbitions;
        this.featureToActions = featureToActions;
        this.actionToGoals = actionToGoals;
        this.featuresSelf = featuresSelf;
    }

    void validate() {
        checkArgument(actionTypes.keySet().containsAll(getSources(actionToGoals)),
                "Missing action types for actions");
        checkArgument(goalAmbitions.keySet().containsAll(getDestinations(actionToGoals)),
                "Missing goal ambitions for goals");
        checkArgument(getSources(actionToGoals).containsAll(getDestinations(featureToActions)),
                "Missing action-goal beliefs for actions");
    }

    private Set<String> getSources(List<Edge> edgeList) {
        return edgeList.stream().map(Edge::getDestination).collect(toSet());
    }

    private Set<String> getDestinations(List<Edge> edgeList) {
        return edgeList.stream().map(Edge::getDestination).collect(toSet());
    }

    List<Feature> getFeaturesSelf() {
        return featuresSelf;
    }

    RecordToken getActionTypes() throws IllegalActionException {
        return STRING_MAP_CONVERTER.convert(actionTypes);
    }

    RecordToken getGoalAmbitions() throws IllegalActionException {
        return DOUBLE_MAP_CONVERTER.convert(goalAmbitions);
    }

    ArrayToken getFeatureToActions() throws IllegalActionException {
        return EDGE_LIST_CONVERTER.convert(featureToActions);
    }

    ArrayToken getActionToGoals() throws IllegalActionException {
        return EDGE_LIST_CONVERTER.convert(actionToGoals);
    }

}
