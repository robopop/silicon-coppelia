package org.bitbucket.marrs.sico.actor.lib.input;

import static org.apache.commons.text.StringEscapeUtils.unescapeJson;
import static org.bitbucket.marrs.sico.actor.lib.input.BeliefPort.ACTION_TO_GOALS;
import static org.bitbucket.marrs.sico.actor.lib.input.BeliefPort.ACTION_TYPES;
import static org.bitbucket.marrs.sico.actor.lib.input.BeliefPort.FEATURES_SELF;
import static org.bitbucket.marrs.sico.actor.lib.input.BeliefPort.FEATURE_TO_ACTIONS;
import static org.bitbucket.marrs.sico.actor.lib.input.BeliefPort.GOAL_AMBITIONS;
import static org.bitbucket.marrs.sico.actor.lib.input.FeaturesPort.AESTHETICS;
import static org.bitbucket.marrs.sico.actor.lib.input.FeaturesPort.AFFORDANCES;
import static org.bitbucket.marrs.sico.actor.lib.input.FeaturesPort.EPISTEMICS;
import static org.bitbucket.marrs.sico.actor.lib.input.FeaturesPort.ETHICS;
import static org.bitbucket.marrs.sico.actor.lib.input.FeaturesPort.FEATURES;
import static org.bitbucket.marrs.sico.actor.lib.input.WeightParameterPort.DECISION_ROW_MAPPING;
import static org.bitbucket.marrs.sico.actor.lib.input.WeightParameterPort.DECISION_WEIGHT_MATRIX;
import static org.bitbucket.marrs.sico.actor.lib.input.WeightParameterPort.DISSIMILARITY_WEIGHTS;
import static org.bitbucket.marrs.sico.actor.lib.input.WeightParameterPort.ENGAGEMENT_ROW_MAPPING;
import static org.bitbucket.marrs.sico.actor.lib.input.WeightParameterPort.ENGAGMENT_WEIGHT_MATRIX;
import static org.bitbucket.marrs.sico.actor.lib.input.WeightParameterPort.IDT_WEIGHT;
import static org.bitbucket.marrs.sico.actor.lib.input.WeightParameterPort.SATISFACTION_WEIGHT;
import static org.bitbucket.marrs.sico.actor.lib.input.WeightParameterPort.SIMILARITY_WEIGHTS;
import static org.bitbucket.marrs.sico.actor.lib.input.WeightParameterPort.USE_INTENTIONS_ROW_MAPPING;
import static org.bitbucket.marrs.sico.actor.lib.input.WeightParameterPort.USE_INTENTIONS_WEIGHT_MATRIX;
import static org.bitbucket.marrs.sico.actor.lib.util.PortUtil.createRecordOutputPort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bitbucket.marrs.sico.actor.lib.util.PortUtil;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import ptolemy.actor.TypedAtomicActor;
import ptolemy.actor.TypedIOPort;
import ptolemy.data.RecordToken;
import ptolemy.data.Token;
import ptolemy.data.type.BaseType;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;
import ptolemy.kernel.util.Workspace;

/**
 * Reads encoded feature weights from JSON data.
 */
public final class JsonEncoder extends TypedAtomicActor {
private static final Gson GSON = new Gson();

    public TypedIOPort rawInput;

    public TypedIOPort featuresOutput;
    public TypedIOPort weightParametersOutput;
    public TypedIOPort beliefsOutput;

    public JsonEncoder(Workspace workspace) throws IllegalActionException, NameDuplicationException {
        super(workspace);
        init();
    }

    public JsonEncoder(CompositeEntity container, String name) throws NameDuplicationException, IllegalActionException {
        super(container, name);
        init();
    }

    private void init() throws IllegalActionException, NameDuplicationException {
        this.rawInput = PortUtil.createTypedInputPort(this, "rawInput", BaseType.STRING);

        this.featuresOutput = createRecordOutputPort(this, "features");
        this.featuresOutput.setTypeEquals(FeaturesPort.getRecordType());
        this.featuresOutput.setDisplayName("Features");
        this.weightParametersOutput = createRecordOutputPort(this, "weightParameters");
        this.weightParametersOutput.setTypeEquals(WeightParameterPort.getRecordType());
        this.weightParametersOutput.setDisplayName("Weight Parameters");
        this.beliefsOutput = createRecordOutputPort(this, "beliefs");
        this.beliefsOutput.setTypeEquals(BeliefPort.getRecordType());
        this.beliefsOutput.setDisplayName("Beliefs");
    }

    @Override
    public void fire() throws IllegalActionException {
        super.fire();

        String rawJsonInput = rawInput.get(0).toString();
        if (rawJsonInput == null || rawJsonInput.isEmpty() || !rawJsonInput.contains("{")) {
            throw new SiliconCoppeliaInputException(this, "No JSON input provided: " + rawJsonInput);
        }

        String jsonInput = normalize(rawJsonInput);
        try {
            Input input = GSON.fromJson(jsonInput, Input.class);

            featuresOutput.send(0, createDomainEncodings(input.getFeatures()));
            beliefsOutput.send(0, createBeliefRecords(input.getBeliefs()));
            weightParametersOutput.send(0, createWeightParameterRecords(input.getWeightParameters()));
        } catch (JsonSyntaxException | IllegalArgumentException e) {
            throw new SiliconCoppeliaInputException(this, e, "Error parsing JSON: " + jsonInput);
        }
    }

    private RecordToken createDomainEncodings(List<Feature> features) throws IllegalActionException {
        Map<String, Token> featureWeights = new HashMap<>();
        Map<String, Token> aestheticsWeights = new HashMap<>();
        Map<String, Token> ethicsWeights = new HashMap<>();
        Map<String, Token> epistemicsWeights = new HashMap<>();
        Map<String, Token> affordancesWeights = new HashMap<>();

        for (Feature feature : features) {
            featureWeights.put(feature.getName(), feature.getWeight());
            aestheticsWeights.put(feature.getName(), feature.getAestheticsWeights());
            ethicsWeights.put(feature.getName(), feature.getEthicsWeights());
            epistemicsWeights.put(feature.getName(), feature.getEpistemicsWeights());
            affordancesWeights.put(feature.getName(), feature.getAffordancesWeights());
        }

        Map<String, Token> domainEncodings = new HashMap<>();
        domainEncodings.put(FEATURES.name, new RecordToken(featureWeights));
        domainEncodings.put(AESTHETICS.name, new RecordToken(aestheticsWeights));
        domainEncodings.put(ETHICS.name, new RecordToken(ethicsWeights));
        domainEncodings.put(EPISTEMICS.name, new RecordToken(epistemicsWeights));
        domainEncodings.put(AFFORDANCES.name, new RecordToken(affordancesWeights));

        return new RecordToken(domainEncodings);
    }

    private RecordToken createBeliefRecords(Beliefs beliefInfo) throws IllegalActionException {
        Map<String, Token> beliefs = new HashMap<>();
        beliefs.put(ACTION_TYPES.name, beliefInfo.getActionTypes());
        beliefs.put(FEATURE_TO_ACTIONS.name, beliefInfo.getFeatureToActions());
        beliefs.put(ACTION_TO_GOALS.name, beliefInfo.getActionToGoals());
        beliefs.put(GOAL_AMBITIONS.name, beliefInfo.getGoalAmbitions());
        beliefs.put(FEATURES_SELF.name, createDomainEncodings(beliefInfo.getFeaturesSelf()));

        return new RecordToken(beliefs);
    }

    private RecordToken createWeightParameterRecords(WeightParameters weightParameterInfo) throws IllegalActionException {
        Map<String, Token> weightParameters = new HashMap<>();
        weightParameters.put(SIMILARITY_WEIGHTS.name, weightParameterInfo.getSimilarityWeights());
        weightParameters.put(DISSIMILARITY_WEIGHTS.name, weightParameterInfo.getDissimilarityWeights());
        weightParameters.put(IDT_WEIGHT.name, weightParameterInfo.getIdtWeight());
        weightParameters.put(SATISFACTION_WEIGHT.name, weightParameterInfo.getSatisfactionWeight());
        weightParameters.put(ENGAGMENT_WEIGHT_MATRIX.name, weightParameterInfo.getEngagementWeightMatrix());
        weightParameters.put(ENGAGEMENT_ROW_MAPPING.name, weightParameterInfo.getEngagementRowMapping());
        weightParameters.put(USE_INTENTIONS_WEIGHT_MATRIX.name, weightParameterInfo.getUseIntentionsWeightMatrix());
        weightParameters.put(USE_INTENTIONS_ROW_MAPPING.name, weightParameterInfo.getUseIntentionsRowMapping());
        weightParameters.put(DECISION_WEIGHT_MATRIX.name, weightParameterInfo.getDecisionWeightMatrix());
        weightParameters.put(DECISION_ROW_MAPPING.name, weightParameterInfo.getDecisionRowMapping());

        return new RecordToken(weightParameters);
    }

    private String normalize(String string) {
        String normalized = unescapeJson(string);
        if (normalized.startsWith("\"") && normalized.endsWith("\"")) {
        normalized = normalized.substring(1, normalized.length() - 1);
        }

return normalized;
    }
}
