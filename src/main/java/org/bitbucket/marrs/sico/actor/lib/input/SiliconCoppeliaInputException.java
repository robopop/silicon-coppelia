package org.bitbucket.marrs.sico.actor.lib.input;

import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.Nameable;

public class SiliconCoppeliaInputException extends IllegalActionException {
    private static final long serialVersionUID = 1L;

    public SiliconCoppeliaInputException(Nameable object1, String detail) {
        super(object1, detail);
    }

    public SiliconCoppeliaInputException(Nameable object1, Throwable cause, String detail) {
        super(object1, cause, detail);
    }

}
