package org.bitbucket.marrs.sico.api;

import static org.apache.commons.text.StringEscapeUtils.escapeJson;
import static org.apache.commons.text.StringEscapeUtils.unescapeJson;

import java.io.InputStream;
import java.lang.invoke.MethodHandles;

import org.bitbucket.marrs.sico.actor.lib.input.SiliconCoppeliaInputException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonParseException;

import ptolemy.actor.CompositeActor;
import ptolemy.actor.Manager;
import ptolemy.actor.TypedCompositeActor;
import ptolemy.data.expr.Parameter;
import ptolemy.data.expr.Variable;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.Workspace;
import ptolemy.moml.MoMLParser;

@RestController
@RequestMapping(path = "api")
public class AppraisalController {
    private static final String JSON_OUTPUT_ATTRIBUTE = "jsonOutput";
    private static final String JSON_INPUT_ATTRIBUTE = "jsonInput";
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static final String MANAGER_PREFIX = "manager-";
    private static final String TEST_SUFFIX = "test";
    private static final String DEFAULT_MODEL = "silicon-coppelia.json.moml";

    @RequestMapping(path = "appraise", method = RequestMethod.GET)
    public void ping() {
        LOGGER.debug("ping");
    }

    @RequestMapping(path = "appraise/{context}", method = RequestMethod.POST,
        consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> appraise(@PathVariable String context,
            @RequestParam(defaultValue = DEFAULT_MODEL) String model,
            HttpEntity<String> httpEntity) {
        String suffix = StringUtils.isEmpty(context) ? TEST_SUFFIX : context;
        String input = httpEntity.getBody();

        LOGGER.debug("Appraisal input for model {}: {}: {}", model, suffix, input);

        Manager manager;
        CompositeActor sicoActor;
        try {
            Workspace workspace = new Workspace();
            CompositeActor container = new TypedCompositeActor(workspace);

            manager = new Manager(workspace, MANAGER_PREFIX + suffix);
            container.setManager(manager);

            sicoActor = getSiliconCoppeliaActor(model, workspace, manager);

            String jsonInput = String.format("\"%s\"", escapeJson(input));
            ((Parameter) sicoActor.getAttribute(JSON_INPUT_ATTRIBUTE)).setExpression(jsonInput);
        } catch (Exception e) {
            LOGGER.error("Failed to initialize the model", e);

            return ResponseEntity.status(500).build();
        }

        try {
            manager.execute();
        } catch (SiliconCoppeliaInputException e) {
            LOGGER.error("Invalid input: " + input, e);

            if (e.getCause() instanceof JsonParseException) {
                return ResponseEntity.badRequest().build();
            } else if (e.getCause() instanceof IllegalArgumentException) {
                return ResponseEntity.unprocessableEntity().build();
            } else {
                return ResponseEntity.badRequest().build();
            }
        } catch (Exception e) {
            LOGGER.error("Failed execute the model", e);

            return ResponseEntity.status(500).build();
        }

        try {
            String result = ((Variable) sicoActor.getAttribute(JSON_OUTPUT_ATTRIBUTE)).getValueAsString();
            result = unescapeJson(result);
            result = org.apache.commons.lang3.StringUtils.strip(result, "\"");

            return ResponseEntity.ok(result); 
        } catch (Exception e) {
            LOGGER.error("Failed to create response", e);

            return ResponseEntity.status(500).build();
        }
    }

    private CompositeActor getSiliconCoppeliaActor(String modelName, Workspace workspace, Manager manager)
            throws Exception, IllegalActionException {
        String modelPath = String.format("/%s.xml", modelName);

        LOGGER.debug("Load model from " + modelPath);

        InputStream momlStream = getClass().getResourceAsStream(modelPath);
        CompositeActor sicoActor = (CompositeActor) new MoMLParser(workspace).parse(null, "Sillicon coppelia", momlStream);
        sicoActor.setManager(manager);

        return sicoActor;
    }
}
