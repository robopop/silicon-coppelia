package org.bitbucket.marrs.sico.algo.compare;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bitbucket.marrs.sico.data.FuzzyWeight;

import java.util.Set;

public final class UtilityResult {
    private final Map<String, FeatureEntry> featureUtilities;
    private final List<ActionGoalTransition> allTransitions;
    private final Set<String> maxUtilityActions;

    UtilityResult(Map<String, FeatureEntry> featureUtilities, List<ActionGoalTransition> allTransitions) {
        this.featureUtilities = featureUtilities;
        this.allTransitions = allTransitions;
        this.maxUtilityActions = new HashSet<>(getFeatureActionResult().values());
    }

    public Map<String, String> getFeatureActionResult() {
        return featureUtilities.entrySet().stream()
                .collect(toMap(Entry::getKey, e -> e.getValue().getMaxUtilityAction()));
    }

    public Map<String, FuzzyWeight> getFeatureUtilitiesResult() {
        return featureUtilities.entrySet().stream()
                .collect(toMap(Entry::getKey, e -> e.getValue().getFeatureUtility()));
    }

    public Map<String, Map<String, Double>> getActionTendenciesResult() {
        return featureUtilities.entrySet().stream()
                .collect(toMap(Entry::getKey, e -> e.getValue().getActionTendencies()));
    }

    public Map<String, List<ActionGoalTransition>> getActionTransitionsResult() {
        return allTransitions.stream()
                .filter(transition -> maxUtilityActions.contains(transition.getActionName()))
                .collect(groupingBy(ActionGoalTransition::getActionName));
    }

    public static final class FeatureEntry {
        private final Map<String, Double> actionTendencies;
        private final String maxUtilityAction;
        private final double maxUtility;
        private final FuzzyWeight featureUtility;

        FeatureEntry(Map<String, Double> actionTendencies, String maxUtilityAction, double maxUtility, FuzzyWeight featureUtility) {
            this.actionTendencies = actionTendencies;
            this.maxUtilityAction = maxUtilityAction;
            this.maxUtility = maxUtility;
            this.featureUtility = featureUtility;
        }

        public Map<String, Double> getActionTendencies() {
            return actionTendencies;
        }

        public String getMaxUtilityAction() {
            return maxUtilityAction;
        }

        public double getMaxUtility() {
            return maxUtility;
        }

        public FuzzyWeight getFeatureUtility() {
            return featureUtility;
        }
    }
}