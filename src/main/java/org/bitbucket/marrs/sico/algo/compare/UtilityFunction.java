package org.bitbucket.marrs.sico.algo.compare;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.bitbucket.marrs.sico.algo.compare.UtilityResult.FeatureEntry;
import org.bitbucket.marrs.sico.algo.util.RecursiveMeanCollector;
import org.bitbucket.marrs.sico.data.FuzzyWeight;

public enum UtilityFunction {
    RECURSIVE_MEAN(RecursiveMeanCollector.INSTANCE),
    AVG(Collectors.averagingDouble(Double::doubleValue));

    private static final Comparator<Entry<String, Double>> UTILITY_VALUE_COMP = (entry1, entry2) ->
            Double.compare(entry1.getValue(), entry2.getValue());

            private final Collector<Double, ?, Double> averagingCollector;

    private UtilityFunction(Collector<Double,?, Double> averagingCollector) {
        this.averagingCollector = averagingCollector;
    }

    public FeatureEntry calculate(List<ActionGoalTransition> transitions) {
        if (transitions.isEmpty()) {
            return null;
        }

        Map<String, Double> utilitesByAction = transitions.stream()
                .collect(groupingBy(trans -> trans.getActionName(),
                        mapping(this::calculateUtility, averagingCollector)));

        Entry<String, Double> maxUtilityAction = utilitesByAction.entrySet().stream()
                .max(UTILITY_VALUE_COMP)
                .get();

        Entry<String, Double> minUtilityAction = utilitesByAction.entrySet().stream()
                .min(UTILITY_VALUE_COMP)
                .get();

        FuzzyWeight featureUtility = new FuzzyWeight(positiveOrZero(maxUtilityAction.getValue()),
                positiveOrZero(minUtilityAction.getValue()));

        return new UtilityResult.FeatureEntry(utilitesByAction,
                maxUtilityAction.getKey(), maxUtilityAction.getValue(),
                featureUtility);
    }

    private Double positiveOrZero(double value) {
        return value > 0 ? value : 0.0;
    }

    private double calculateUtility(ActionGoalTransition transition) {
        return transition.getGoalAmbition()
                * (transition.getTransitionWeight().getPositiveWeight()
                        - transition.getTransitionWeight().getNegativeWeight());
    }
}
