package org.bitbucket.marrs.sico.algo.compare;

import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.Math.sqrt;
import static org.apache.commons.math3.linear.MatrixUtils.createRealDiagonalMatrix;

import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public class SimilarityDistanceFunction {

    private final RealMatrix distanceForm;

    public static SimilarityDistanceFunction forFeatureIntersection(double[] parameters) {
        return new IntersectingDistanceFunction(parameters);
    }

    public static SimilarityDistanceFunction forFeatureUnion(double[] parameters) {
        return new SimilarityDistanceFunction(parameters);
    }

    private SimilarityDistanceFunction(double[] parameters) {
        this(createRealDiagonalMatrix(parameters));
    }

    public SimilarityDistanceFunction(RealMatrix distanceForm) {
        this.distanceForm = distanceForm;
    }

    public double apply(RealVector other, RealVector self) {
        return apply(other, self, true, true);
    }

    public double apply(RealVector other, RealVector self, boolean isFeatureOfOther, boolean isFeatureOfSelf) {
        checkArgument(0.0 <= other.getMinValue() && other.getMaxValue() <= 1.0, "values must be between 0 and 1: %s", other);
        checkArgument(0.0 <= self.getMinValue() && self.getMaxValue() <= 1.0, "values must be between 0 and 1: %s", self);

        RealVector diff = other.subtract(self);

        return sqrt(diff.dotProduct(distanceForm.operate(diff)) / diff.getDimension());
    }

    private static class IntersectingDistanceFunction extends SimilarityDistanceFunction {
        public IntersectingDistanceFunction(double[] distanceForm) {
            super(distanceForm);
        }

        @Override
        public double apply(RealVector other, RealVector self, boolean isFeatureOfOther, boolean isFeatureOfSelf) {
            if (isFeatureOfOther ^ isFeatureOfSelf) {
                return 1.0;
            }

            return super.apply(other, self, isFeatureOfOther, isFeatureOfSelf);
        }
    }
}
