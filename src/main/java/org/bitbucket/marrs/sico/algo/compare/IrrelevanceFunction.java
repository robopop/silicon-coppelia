package org.bitbucket.marrs.sico.algo.compare;

import static java.lang.Math.abs;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.util.Comparator.naturalOrder;
import static java.util.stream.Collectors.groupingBy;

import java.util.List;

public enum IrrelevanceFunction {
    INSTANCE;

    public double calculate(List<ActionGoalTransition> transitions) {
        return transitions.stream()
                .collect(groupingBy(t -> t.getGoalName())).values().stream()
                .map(this::goalIrrelevance)
                .max(naturalOrder())
                .orElse(0.0);
    }

    private double goalIrrelevance(List<ActionGoalTransition> transitionsForGoal) {
        if (transitionsForGoal.isEmpty()) {
            return 0.0;
        }

        double goalImportance = abs(transitionsForGoal.get(0).getGoalAmbition());
        double anyAffects = transitionsForGoal.stream()
                .map(this::affects)
                // or
                .max(naturalOrder())
                .orElse(0.0);

        return 1 - and(anyAffects, goalImportance);
    }

    private double affects(ActionGoalTransition transition) {
        return max(abs(transition.getTransitionWeight().getPositiveWeight()),
                abs(transition.getTransitionWeight().getNegativeWeight()));
    }

    private double and(double val1, double val2) {
        return min(val1, val2);
    }
}
