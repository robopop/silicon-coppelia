package org.bitbucket.marrs.sico.algo.compare;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

import org.bitbucket.marrs.sico.data.Edge;

public final class UtilityAlgo {
    private Map<String, List<Edge>> featureActionAssociationsInput;
    private Map<String, String> actionTypesInput;
    private Map<String, List<Edge>> actionGoalTransitionsInput;
    private Map<String, Double> goalMetaInput;

    public UtilityAlgo(Map<String, List<Edge>> featureActionAssociationsInput,
            Map<String, String> actionTypesInput,
            Map<String, List<Edge>> actionGoalTransitionsInput,
            Map<String, Double> goalAmbitionsInput) {
        this.featureActionAssociationsInput = featureActionAssociationsInput;
        this.actionTypesInput = actionTypesInput;
        this.actionGoalTransitionsInput = actionGoalTransitionsInput;
        this.goalMetaInput = goalAmbitionsInput;
    }

    public UtilityResult calcluateUtility(Set<String> features) {
        Map<String, UtilityResult.FeatureEntry> featureUtilities = new HashMap<>();
        List<ActionGoalTransition> allTransitions = new ArrayList<>();
        Set<String> maxActionNames = new HashSet<>();
        for (String featureName : features) {
            List<ActionGoalTransition> transitions = featureActionAssociationsInput.get(featureName).stream()
                    .flatMap(resolveTransitions(actionGoalTransitionsInput, actionTypesInput, goalMetaInput))
                    .collect(toList());
            if (transitions.isEmpty()) {
                continue;
            }

            allTransitions.addAll(transitions);

            UtilityResult.FeatureEntry utilityResult = UtilityFunction.AVG.calculate(transitions);
//            UtilityResult.FeatureEntry utilityResult = UtilityFunction.RECURSIVE_MEAN.calculate(transitions);
            featureUtilities.put(featureName, utilityResult);
            maxActionNames.add(utilityResult.getMaxUtilityAction());
        }

        return new UtilityResult(featureUtilities, allTransitions);
    }

    private Function<Edge, Stream<ActionGoalTransition>> resolveTransitions(
            Map<String, List<Edge>> actionGoalTransitions,
            Map<String, String> actionTypes,
            Map<String, Double> goalAmbitions) {
        return association -> {
//                String actionName = association.getNode(actionTypes.keySet());
                String actionName = association.getDestination();
                double truthValue = association.getTruthValue();

                return actionGoalTransitions.get(actionName).stream()
                        .map(transition -> new ActionGoalTransition(transition, actionTypes, goalAmbitions, truthValue));
        };
    }
}
