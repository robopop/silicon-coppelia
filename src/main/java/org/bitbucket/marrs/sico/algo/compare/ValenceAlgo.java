package org.bitbucket.marrs.sico.algo.compare;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.bitbucket.marrs.sico.algo.compare.ValenceFunction.ValenceResult;
import org.bitbucket.marrs.sico.data.Edge;
import org.bitbucket.marrs.sico.data.FuzzyWeight;

import ptolemy.kernel.util.IllegalActionException;

public final class ValenceAlgo {
    private final Map<String, List<Edge>> featureActionAssociations;
    private final Map<String, String> actionTypes;
    private final Map<String, List<Edge>> actionGoalTransitions;
    private final Map<String, Double> goalAmbitions;

    public ValenceAlgo(Map<String, List<Edge>> featureActionAssociations,
            Map<String, String> actionTypes,
            Map<String, List<Edge>> actionGoalTransitions,
            Map<String, Double> goalAmbitions) {
        this.featureActionAssociations = featureActionAssociations;
        this.actionTypes = actionTypes;
        this.actionGoalTransitions = actionGoalTransitions;
        this.goalAmbitions = goalAmbitions;
    }

    public FuzzyWeight calculateValence(String feature, FuzzyWeight affordances) throws IllegalActionException {
        List<ActionGoalTransition> transitions = featureActionAssociations.get(feature).stream()
                .flatMap(association -> resolveTransitions(association))
                .collect(toList());

        if (transitions.isEmpty()) {
            return FuzzyWeight.ZERO;
        }

        ValenceResult valenceResult = ValenceFunction.INSTANCE
                .calculate(affordances.getPositiveWeight(), affordances.getNegativeWeight(), transitions);

        return new FuzzyWeight(valenceResult.getPositive(), valenceResult.getNegative());
    }

    private Stream<ActionGoalTransition> resolveTransitions(Edge association) {
//      TODO  String actionName = association.getNode(actionTypes.keySet());
        String actionName = association.getDestination();
        double truthValue = association.getTruthValue();

        return actionGoalTransitions.get(actionName).stream()
                .map(transition -> new ActionGoalTransition(transition, actionTypes, goalAmbitions, truthValue));
    }
}
