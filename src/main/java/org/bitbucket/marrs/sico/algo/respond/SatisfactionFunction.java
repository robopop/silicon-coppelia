package org.bitbucket.marrs.sico.algo.respond;


public class SatisfactionFunction {
    private final double weight;

    public SatisfactionFunction(double weight) {
        this.weight = weight;
    }

    public double apply(double idt, double useIntention) {
        return weight * idt + (1 - weight) * useIntention;
    }
}
