package org.bitbucket.marrs.sico.data.type;

import org.bitbucket.marrs.sico.data.Edge;
import org.bitbucket.marrs.sico.data.token.EdgeToken;

import ptolemy.data.ObjectToken;
import ptolemy.data.Token;
import ptolemy.data.type.ObjectType;
import ptolemy.data.type.Type;
import ptolemy.kernel.util.IllegalActionException;

public class EdgeType extends ObjectType {
    public static final EdgeType EDGE = new EdgeType();

    private EdgeType() {
        super(Edge.class);
    }

    @Override
    public boolean isCompatible(Type type) {
        return super.isCompatible(type) && Edge.class.isInstance(((EdgeType) type).getTokenClass());
    }

    @Override
    public EdgeToken convert(Token token) throws IllegalActionException {
        ObjectToken converted = (ObjectToken) super.convert(token);

        if (Edge.class.isInstance(converted.getValueClass())) {
            throw new IllegalActionException("Cannot convert to Edge token, invalid type: " + converted.getValueClass());
        }

        return new EdgeToken((Edge) converted.getValue());
    }
}
