package org.bitbucket.marrs.sico.data;

/**
 * Relation between 2 concepts in a concept-graph.
 * A relation is undirected, is identified by its 2 concepts and had a truth value.
 * The equals and hashCode are overwritten to work on only the identifier(s).
 */
public final class Edge {

    private final String source;
    private final String destination;
    private final double strength;
    private final double truthValue;

    public Edge(String source, String destination, double strength, double truthValue) {
        this.source = source;
        this.destination = destination;
        this.strength = strength;
        this.truthValue = truthValue;
    }

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    public double getStrength() {
        return strength;
    }

    public double getTruthValue() {
        return truthValue;
    }

    public String getOtherNode(String node) {
        if (node.equals(source)) {
            return destination;
        }
        if (node.equals(destination)) {
            return source;
        }
        throw new IllegalArgumentException(String.format("Node %s not part of association %s", node, this));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Edge)) return false;

        Edge that = (Edge) o;

        if (!source.equals(that.source)) return false;
        if (!destination.equals(that.destination)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = source.hashCode();
        result = 31 * result + destination.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s -%.2f- %s", source, truthValue, destination);
    }
}
