package org.bitbucket.marrs.sico;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.lang.invoke.MethodHandles;
import java.util.Arrays;

@SpringBootApplication
public class SiliconCoppeliaApplication {
    private static final Class<?> CLASS = MethodHandles.lookup().lookupClass();
    private static final Logger LOGGER = LoggerFactory.getLogger(CLASS);

    public static void main(String[] args) {
        SpringApplication.run(CLASS, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {

            LOGGER.info("Let's inspect the beans provided by Spring Boot:");

            String[] beanNames = ctx.getBeanDefinitionNames();
            LOGGER.info("Number of beans: {}", beanNames.length);
            if (LOGGER.isTraceEnabled()) {
                Arrays.sort(beanNames);
                for (String beanName : beanNames) {
                    LOGGER.trace("Bean: {}", beanName);
                }
            }
        };
    }
}
