package org.bitbucket.marrs.sico.itest;

import static java.util.stream.Collectors.joining;
import static org.apache.commons.text.StringEscapeUtils.escapeJson;
import static org.apache.commons.text.StringEscapeUtils.unescapeJson;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

import org.bitbucket.marrs.sico.actor.lib.test.ActorBaseTest;
import org.junit.Test;

import ptolemy.actor.CompositeActor;
import ptolemy.actor.Manager;
import ptolemy.data.expr.Parameter;
import ptolemy.data.expr.Variable;
import ptolemy.kernel.util.Workspace;
import ptolemy.moml.MoMLParser;


public final class ModelExecutionTest extends ActorBaseTest {
    private static final String INPUT = new BufferedReader(new InputStreamReader(ModelExecutionTest.class.getResourceAsStream("/input.json")))
            .lines()
            .collect(joining());

    @Test
    public void momlModelExecutes() throws Exception {
        Workspace workspace = getWorkspace();

        String jsonInput = String.format("\"%s\"", escapeJson(INPUT));

        InputStream momlStream = getClass().getResourceAsStream("/silicon-coppelia.json.moml.xml");
        CompositeActor model = (CompositeActor) new MoMLParser(workspace).parse(null, "test model", momlStream);
        ((Parameter) model.getAttribute("jsonInput")).setExpression(jsonInput);

        Manager manager = getManager();
        model.setManager(manager);
        executeModel();

        String result = unescapeJson(((Variable) model.getAttribute("jsonOutput")).getValueAsString());

        assertTrue(result.matches(".*\"Action\":\\s*\"return\".*"));
    }

    @Test
    public void ioModelExecutes() throws Exception {
        PrintStream sysout = System.out;

        try {
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            PrintStream printStream = new PrintStream(outStream);
            System.setOut(printStream);

            Workspace workspace = getWorkspace();

            InputStream momlStream = getClass().getResourceAsStream("/silicon-coppelia.json.io.xml");
            CompositeActor model = (CompositeActor) new MoMLParser(workspace).parse(null, "test model", momlStream);
            ((Parameter) model.getAttribute("in")).setExpression(getClass().getResource("/input.json").getPath());

            Manager manager = getManager();
            model.setManager(manager);
            executeModel();

            assertTrue(outStream.toString().trim().matches(".*\"Action\":\\s*\"return\".*"));
        } finally {
            System.setOut(sysout);
        }
    }

}