package org.bitbucket.marrs.sico.algo.respond;

import static junit.framework.Assert.assertEquals;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;


public final class SatisfactionFunctionTest {

    private static final double DELTA = 1e-15;
    private static final long SEED = 2_603_886_880_581_722L;

    private Random random;

    @Before
    public void setupSeed() {
        random = new Random(SEED);
    }

    @Test
    public void equalWeightEqualValues() {
        SatisfactionFunction satisfaction = new SatisfactionFunction(0.5);

        assertEquals(0.5, satisfaction.apply(0.5, 0.5), DELTA);
    }

    @Test
    public void equalWeightBoundaryValues() {
        SatisfactionFunction satisfaction = new SatisfactionFunction(0.5);

        assertEquals(0.0, satisfaction.apply(0.0, 0.0), DELTA);
        assertEquals(0.5, satisfaction.apply(1.0, 0.0), DELTA);
        assertEquals(0.5, satisfaction.apply(0.0, 1.0), DELTA);
        assertEquals(1.0, satisfaction.apply(1.0, 1.0), DELTA);
    }

    @Test
    public void linearity() {
        for (int i = 0; i < 100; i++) {
            double weight = random.nextDouble();
            SatisfactionFunction satisfaction = new SatisfactionFunction(weight);

            double idtExtreme = satisfaction.apply(1, 0);
            double uiExtreme = satisfaction.apply(0, 1);

            for (int j = 0; j < 100; j++) {
                double idt = random.nextDouble();
                double ui = random.nextDouble();

                assertEquals("Failed with seed: " + SEED,
                    idt * idtExtreme + ui * uiExtreme, satisfaction.apply(idt, ui), DELTA);
            }
        }
    }
}
