package org.bitbucket.marrs.sico.actor.lib.test;

import java.util.Map;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import ptolemy.data.RecordToken;
import ptolemy.data.Token;
import ptolemy.kernel.util.IllegalActionException;

public final class RecordMatcher extends BaseMatcher<RecordToken> {

    private final Map<String, Token> expectedEntries;

    public RecordMatcher(Map<String, Token> expectedEntries) {
        this.expectedEntries = expectedEntries;
    }

    public static RecordMatcher hasEntries(Map<String, Token> expectedEntries) {
        return new RecordMatcher(expectedEntries);
    }

    @Override
    public boolean matches(Object actual) {
        if (!(actual instanceof Token)) {
            return false;
        }

        if (expectedEntries == null) {
            return ((Token) actual).isNil();
        }

        if (!(actual instanceof RecordToken)) {
            return false;
        }

        RecordToken actualRecord = (RecordToken) actual;

        try {
            return actualRecord.equals(new RecordToken(expectedEntries));
        } catch (IllegalActionException e) {
            throw new RuntimeException("Test failed", e);
        }
    }

    @Override
    public void describeTo(Description description) {
        description.appendValue(expectedEntries);
    }
}