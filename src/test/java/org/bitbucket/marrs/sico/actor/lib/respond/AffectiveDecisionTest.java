package org.bitbucket.marrs.sico.actor.lib.respond;

import static junit.framework.Assert.assertEquals;

import org.bitbucket.marrs.sico.actor.lib.test.ActorBaseTest;
import org.bitbucket.marrs.sico.actor.lib.test.PortListener;
import org.junit.Ignore;
import org.junit.Test;

import ptolemy.data.StringToken;
import ptolemy.data.Token;
import ptolemy.kernel.util.KernelException;

public final class AffectiveDecisionTest extends ActorBaseTest {

    @Test
    @Ignore
    public void actorExecutes() throws KernelException {
        AffectiveDecision decisionContainer = new AffectiveDecision(getContainer(), "testADContainer");

        feedConstantInput(decisionContainer, decisionContainer.useIntentionsInput, "{ \"feature\" = { \"indicative\" = 1.0, \"counter-indicative\" = 0.0 } }");
        feedConstantInput(decisionContainer, decisionContainer.tradeoffInput, "{ \"feature\" = 1.0 }");
        feedConstantInput(decisionContainer, decisionContainer.actionsInput, "{ \"feature\" = \"test\" }");
        feedConstantInput(decisionContainer, decisionContainer.actionTendenciesInput, "{ \"feature\" = \"test\" }");
        feedConstantInput(decisionContainer, decisionContainer.engagementInput, "{ \"feature\" = \"test\" }");
        feedConstantInput(decisionContainer, decisionContainer.satisfactionWeightInput, "0.5");
        feedConstantInput(decisionContainer, decisionContainer.decisionRowMappingInput, "{ \"feature\" = \"test\" }");
        feedConstantInput(decisionContainer, decisionContainer.decisionWeightMatrixInput, "{ \"feature\" = \"test\" }");

        PortListener<Token> actionOutput = terminateOutput(decisionContainer, decisionContainer.actionOutput);
        PortListener<Token> satisfactionOutput = terminateOutput(decisionContainer, decisionContainer.satisfactionOutput);
        PortListener<Token> satisfactionTotalOutput = terminateOutput(decisionContainer, decisionContainer.satisfactionTotalOutput);
        PortListener<Token> featureSatisfactionOutput = terminateOutput(decisionContainer, decisionContainer.featureSatisfactionOutput);

        executeModel();

        assertEquals("test", StringToken.convert(actionOutput.getToken()).stringValue());
    }
}
