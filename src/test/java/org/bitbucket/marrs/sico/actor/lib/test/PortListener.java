package org.bitbucket.marrs.sico.actor.lib.test;

import ptolemy.actor.IOPortEvent;
import ptolemy.actor.IOPortEventListener;
import ptolemy.data.Token;
import ptolemy.kernel.util.IllegalActionException;

public final class PortListener<T extends Token> implements IOPortEventListener {

    private T token;
    private int invocations = 0;

    @Override
    @SuppressWarnings("unchecked")
    public synchronized void portEvent(IOPortEvent event) throws IllegalActionException {
        token = (T) event.getToken();
        invocations++;
    }

    public T getToken() {
        return token;
    }

    public int getInvocations() {
        return invocations;
    }
}