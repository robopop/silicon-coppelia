Silicon Coppélia
================

*Silicon Coppélia* is a software for modeling affective decision making.

In the following you can find instructions on several aspects of using the software.

## Invoking models and the *Ptolemy* framework from the command line

*Silicon Coppélia* is implemented as a model in the *Ptolemy* framework and the software distribution contains support to start the *Ptolemy* graphical interface and to run *Ptolemy* models from the command line:

* Create the *Silicon Coppélia* distribution by executing the gradle build from the root directory of the repository:

        > ./gradlew clean distZip

    This will create the distribution file the following distribution file:

        sico/build/distributions/sico-VERSION.zip

* Copy the generated zip file to a destination of your choice. Extracting the zip file will create a

        mydir/sico-VERSION

    folder.

* The extracted folder contains the following executable files:
    To run the graphical interface execute

        > ./mydir/sico-VERSION/bin/sico

    To execute an arbitrary *Ptolemy* model from the command line execute

        > ./mydir/sico-VERSION/bin/sicoCmd mymodel.xml

    To run the *Silicon Coppélia* model from the command line a json file containing the input to the model and a folder containing the necessary configuration files need to be specified as additional input parameters:

        > ./mydir/sico-VERSION/bin/sicoCmd -input myinput.json silicon-coppelia.io.xml

    To avoid issues related to the path resolution of the files relative to the location were the command is executed it is recommended to use absolute paths in the above command.


## Using a composite actor in an other Workspace

When exporting an actor to the library, layout and connections get lost in some cases. The best strategy to reuse the actor is to copy paste the actor into a new composite actor in the new workspace.
